<?php 

function set_user_data($data='')
{
	$CI = & get_instance();
	$CI->load->library('session');

	$CI->session->set_userdata($data);
}

// dados arena
function get_user_data()
{
	$CI = & get_instance();
	$CI->load->library('session');
    
    return $CI->session->userdata();
}

// id arena
function get_userID_arena_0()
{
	$CI = & get_instance();
	$CI->load->library('session');
    
    return $CI->session->userdata('arena_0')['SESSION_ID'];
}
function get_userID_arena_1()
{
	$CI = & get_instance();
	$CI->load->library('session');
    
    return $CI->session->userdata('arena_1')['SESSION_ID'];
}
function get_userID_arena_2()
{
	$CI = & get_instance();
	$CI->load->library('session');
    
    return $CI->session->userdata('arena_2')['SESSION_ID'];
}

// webservices
function get_webservice_0()
{
	$CI = & get_instance();
	$CI->load->library('session');
    
    return $CI->session->userdata('webservice_0');
}
function get_webservice_1()
{
	$CI = & get_instance();
	$CI->load->library('session');
    
    return $CI->session->userdata('webservice_1');
}
function get_webservice_2()
{
	$CI = & get_instance();
	$CI->load->library('session');
    
    return $CI->session->userdata('webservice_2');
}
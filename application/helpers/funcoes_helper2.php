<?php
date_default_timezone_set('America/Sao_Paulo');
function converteData($datahora){
// data = 2012-01-01 12:00:00
	if($datahora!=""){
	$hora="";
	$datahora = explode(" ",$datahora);
	$data = explode("-",$datahora[0]);
	if(isset($datahora[1]))
		$hora = " ".$datahora[1];
	
		return $data[2]."/".$data[1]."/".$data[0].$hora;
	}
	else
		return "";
}

function converteDataBD($datahora){
// data = 2012/01/01 12:00:00
	if($datahora!=""){
	//	echo "dt:".$datahora."<br>";
		$hora="";
		$datahora = explode(" ",$datahora);
		$data = explode("/",$datahora[0]);
		if(isset($datahora[1]) && isset($data[1])){
			$hora = " ".$datahora[1];
	
			return $data[2]."-".$data[1]."-".$data[0].$hora;
		}
	}
	else
		return "";
}

function somahoras($h1, $h2){
	// 00:00:00
	$arrh1 = explode(":",$h1);
	$arrh2 = explode(":",$h2);
	$seg = $arrh1[2] + $arrh2[2];
	$min = $arrh1[1] + $arrh2[1];
	$hor = $arrh1[0] + $arrh2[0];
	
	if($seg>59){
		$seg = $seg - 60;
		$min++;	
	}
	
	if($min>59){
		$min= $min - 60;
		$hor++;
	}
	
	if($seg<10)
		$seg = "0".$seg;
	if($min<10)
		$min = "0".$min;
	if($hor<10)
		$hor = "0".$hor;
	
	return $hor.":".$min.":".$seg;
}
// seed with microseconds
function make_seed() {
  list($usec, $sec) = explode(' ', microtime());
  return (float) $sec + ((float) $usec * 100000);
}

// número randômico
function random($min,$max){
	srand(make_seed());
	return rand($min,$max);
}

// adiciona dias em uma data
function addDay($date,$days) {
	$data = explode("-", $date);
	$nextdate = mktime ( 0, 0, 0, $data[1], $data[2] + $days, $data[0] );
	return strftime("%Y-%m-%d", $nextdate);
}

// calcula quantos dias tem entre duas datas
function calculaIntervalo($data1,$data2){
	// separa as datas em dia,mes e ano
	list($ano1,$mes1,$dia1) = explode("-",$data1);
	list($ano2,$mes2,$dia2) = explode("-",$data2);

	// so lembrando que o padrao eh MM/DD/AAAA
	$timestamp1 = mktime(0,0,0,$mes1,$dia1,$ano1);
	$timestamp2 = mktime(0,0,0,$mes2,$dia2,$ano2);

	// calcula a diferenca em timestamp
	$diferenca = ($timestamp1 > $timestamp2) ? ($timestamp1 - $timestamp2) : ($timestamp2 - $timestamp1);

	// retorna o calculo em anos, meses e dias
	return (date("d",$diferenca)-1);
}

// transforma Sim/Nao em inteiro pra texto
function SimNaoTexto($Valor){
	if (intval($Valor) == 0)
		return ('NÃO');
	else if (intval($Valor) == 1)
		return ('SIM');
	return;
}

// pega uma direçao em inteiro e transforma em texto
function DirecaoTexto($Direcao){
	switch ($Direcao){
		case 0: return "NORTE";
		case 1: return "NORDESTE";
		case 2: return "LESTE";
		case 3: return "SUDESTE";
		case 4: return "SUL";
		case 5: return "SUDOESTE";
		case 6: return "OESTE";
		case 7: return "NOROESTE";
	}	
}

// verifica se o XML retornou um erro
function verificaErro($data){
  $parser = xml_parser_create();
  xml_parser_set_option($parser,XML_OPTION_CASE_FOLDING,0);
  xml_parser_set_option($parser,XML_OPTION_SKIP_WHITE,1);
  xml_parse_into_struct($parser,$data,$values,$tags);
  xml_parser_free($parser);
  if (count($values) == 1)
  	if ($values[0]["tag"] == "ERROR")
  		return $values[0]["value"];
  return;
}

// interpreta um XML
function readXML1($data,$Raiz) {
  $parser = xml_parser_create();
  xml_parser_set_option($parser,XML_OPTION_CASE_FOLDING,0);
  xml_parser_set_option($parser,XML_OPTION_SKIP_WHITE,1);
  xml_parse_into_struct($parser,$data,$values,$tags);
  xml_parser_free($parser);

  foreach ($tags as $key=>$val){
    if ($key == $Raiz){
      $molranges = $val;
      for ($i = 0; $i < count($molranges); $i += 2){
		if(isset($molranges[$i+1])){
			$offset = $molranges[$i] + 1;
			$len    = $molranges[$i + 1] - $offset;
			$tdb[]  = parseXML1(array_slice($values,$offset,$len));
		}
      }
    }else
      continue;
  }
  if(isset($tdb[0]))
	return $tdb[0];
}

function parseXML1($mvalues){
	$j = 0;
	$mol = array();
	for ($i = 0; $i < count($mvalues); $i++){
  	if ($mvalues[$i]["type"] == "complete"){
  		if(isset($mvalues[$i]["value"]))
			$mol[$j/2][$mvalues[$i]["tag"]] = $mvalues[$i]["value"];
		
	}
  	else if ($mvalues[$i]["type"] == "close")
  		$j++;
	}
	return ($mol);
}

function readXML($data,$Raiz) {
  $tdb = array();
  $parser = xml_parser_create();
  xml_parser_set_option($parser,XML_OPTION_CASE_FOLDING,0);
  xml_parser_set_option($parser,XML_OPTION_SKIP_WHITE,1);
  xml_parse_into_struct($parser,$data,$values,$tags);
  xml_parser_free($parser);

  foreach ($tags as $key=>$val){
    if ($key == $Raiz){
      $molranges = $val;
      for ($i = 0; $i < count($molranges); $i += 2){
        $offset = $molranges[$i] + 1;
		if(isset($molranges[$i + 1]))
			$len    = $molranges[$i + 1] - $offset;
		else
			$len = $offset;
        $tdb[]  = parseXML(array_slice($values,$offset,$len));
      }
    }else
      continue;
  }
  if(isset($tdb[0]))
	return $tdb[0];
}

function parseXML($mvalues){
	$j = 0;
	$mol = array();
	for ($i = 0; $i < count($mvalues); $i++){
  	if ($mvalues[$i]["type"] == "complete"){
		if(isset($mvalues[$i]["value"]))
			$mol[$j][$mvalues[$i]["tag"]] = $mvalues[$i]["value"];		
	}
  	else if ($mvalues[$i]["type"] == "close")
  		$j++;
	}
	return ($mol);
}



// funçoes auxiliares para formataçao de datas e números

define("DEFAULT_CURRENCY_SYMBOL", "R$");
define("DEFAULT_MON_DECIMAL_POINT", ",");
define("DEFAULT_MON_THOUSANDS_SEP", ".");
define("DEFAULT_POSITIVE_SIGN", "");
define("DEFAULT_NEGATIVE_SIGN", "-");
define("DEFAULT_FRAC_DIGITS", 2);
define("DEFAULT_P_CS_PRECEDES", true);
define("DEFAULT_P_SEP_BY_SPACE", false);
define("DEFAULT_N_CS_PRECEDES", true);
define("DEFAULT_N_SEP_BY_SPACE", false);
define("DEFAULT_P_SIGN_POSN", 3);
define("DEFAULT_N_SIGN_POSN", 3);

// PHPMaker DEFAULT_DATE_FORMAT: "yyyy/mm/dd"(default)  or "mm/dd/yyyy" or "dd/mm/yyyy"
define("DEFAULT_DATE_FORMAT", "dd/mm/yyyy");

//---------------------------------------------------------------------------------------------------------
// format a timestamp, datetime, date or time field from MySQL
// $namedformat: 0 - General Date, 1 - Long Date, 2 - Short Date, 3 - Long Time, 4 - Short Time 5 - More Short Time
function FormatDateTime($ts, $namedformat)
{
  $DefDateFormat = str_replace("yyyy", "%Y", DEFAULT_DATE_FORMAT);
	$DefDateFormat = str_replace("mm", "%m", $DefDateFormat);
	$DefDateFormat = str_replace("dd", "%d", $DefDateFormat);
	
	if (is_numeric($ts)) // timestamp
	{
		switch (strlen($ts)) {
		    case 14:
		        $patt = '/(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/';
		        break;
		    case 12:
		        $patt = '/(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/';
		        break;
		    case 10:
		        $patt = '/(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/';
		        break;
			case 8:
		        $patt = '/(\d{4})(\d{2})(\d{2})/';
		        break;
			case 6:
		        $patt = '/(\d{2})(\d{2})(\d{2})/';
		        break;
			case 4:
		        $patt = '/(\d{2})(\d{2})/';
		        break;
			case 2:
		        $patt = '/(\d{2})/';
		        break;
			default:
				return $ts;
		}		
		if ((isset($patt))&&(preg_match($patt, $ts, $matches)))
		{
			$year = $matches[1];
			$month = @$matches[2];
			$day = @$matches[3];
			$hour = @$matches[4];
			$min = @$matches[5];
			$sec = @$matches[6];
		}
		if (($namedformat==0)&&(strlen($ts)<10)) $namedformat = 2;
	}
	elseif (is_string($ts))
	{		
		if (preg_match('/(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/', $ts, $matches)) // datetime
		{
			$year = $matches[1];
			$month = $matches[2];
			$day = $matches[3];
			$hour = $matches[4];
			$min = $matches[5];
			$sec = $matches[6];
		}
		elseif (preg_match('/(\d{4})-(\d{2})-(\d{2})/', $ts, $matches)) // date
		{
			$year = $matches[1];
			$month = $matches[2];
			$day = $matches[3];
			if ($namedformat==0) $namedformat = 2;
		}
		elseif (preg_match('/(^|\s)(\d{2}):(\d{2}):(\d{2})/', $ts, $matches)) // time
		{
			$type = "time";			
			$hour = $matches[2];
			$min = $matches[3];
			$sec = $matches[4];
			if (($namedformat==0)||($namedformat==1)) $namedformat = 3;
			if ($namedformat==2) $namedformat = 4;
		}
		else
		{
			return $ts;
		}
	}
	else
	{
		return $ts;
	}
	
	if (!isset($year)) $year = 0; // dummy value for times
	if (!isset($month)) $month = 1;
	if (!isset($day)) $day = 1;	
	if (!isset($hour)) $hour = 0;
	if (!isset($min)) $min = 0;
	if (!isset($sec)) $sec = 0;
	
	$uts = mktime($hour, $min, $sec, $month, $day, $year);
	
	if ($uts==-1) return $ts; // fail to convert
	
	switch ($namedformat) {
    case 0:
        return strftime($DefDateFormat." %H:%M:%S", $uts);
        break;
    case 1:
        return strftime("%A, %B %d, %Y", $uts);		
        break;
    case 2:
        return strftime($DefDateFormat, $uts);
        break;
	case 3:
        return strftime("%H:%M:%S %p", $uts);
        break;
	case 4:
        return strftime("%H:%M:%S", $uts);
        break;
	case 5:
        return strftime("%H:%M", $uts);
        break;
	}
}

//---------------------------------------------------------------------------------------------------------
// Convert a date to MySQL format
function ConvertDateToMysqlFormat($dateStr)
{
    switch (DEFAULT_DATE_FORMAT) {
    case "yyyy/mm/dd":
        list ($year, $month, $day) = split("/", $dateStr);
        break;
    case "mm/dd/yyyy":
        list ($month, $day, $year) = split("/", $dateStr);
        break;
    case "dd/mm/yyyy":
        list ($day, $month, $year) = split("/", $dateStr);
        break;
	}
    return $year . "-" . $month . "-" . $day;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------
// FormatCurrency(Expression[,NumDigitsAfterDecimal [,IncludeLeadingDigit [,UseParensForNegativeNumbers [,GroupDigits]]]])
//
// NumDigitsAfterDecimal is the numeric value indicating how many places to the right of the decimal are displayed
// -1 Use Default
//
// The IncludeLeadingDigit, UseParensForNegativeNumbers, and GroupDigits arguments have the following settings:
// -1 True 
// 0 False 
// -2 Use Default

function FormatCurrency($amount, $NumDigitsAfterDecimal, $IncludeLeadingDigit, $UseParensForNegativeNumbers, $GroupDigits) 
{
    //Export the values returned by localeconv into the local scope
    extract(localeconv());
	
	// Set defaults if locale is not set
	if (empty($currency_symbol)) $currency_symbol = DEFAULT_CURRENCY_SYMBOL;
	if (empty($mon_decimal_point)) $mon_decimal_point = DEFAULT_MON_DECIMAL_POINT;
	if (empty($mon_thousands_sep)) $mon_thousands_sep = DEFAULT_MON_THOUSANDS_SEP;
	if (empty($positive_sign)) $positive_sign = DEFAULT_POSITIVE_SIGN;
	if (empty($negative_sign)) $negative_sign = DEFAULT_NEGATIVE_SIGN;
	if ($frac_digits == CHAR_MAX) $frac_digits = DEFAULT_FRAC_DIGITS;
	if ($p_cs_precedes == CHAR_MAX) $p_cs_precedes = DEFAULT_P_CS_PRECEDES;
	if ($p_sep_by_space == CHAR_MAX) $p_sep_by_space = DEFAULT_P_SEP_BY_SPACE;
	if ($n_cs_precedes == CHAR_MAX) $n_cs_precedes = DEFAULT_N_CS_PRECEDES;
	if ($n_sep_by_space == CHAR_MAX) $n_sep_by_space = DEFAULT_N_SEP_BY_SPACE;
	if ($p_sign_posn == CHAR_MAX) $p_sign_posn = DEFAULT_P_SIGN_POSN;
	if ($n_sign_posn == CHAR_MAX) $n_sign_posn = DEFAULT_N_SIGN_POSN;
	
	// check $NumDigitsAfterDecimal
	if ($NumDigitsAfterDecimal > -1) 
		$frac_digits = $NumDigitsAfterDecimal;
	
	// check $UseParensForNegativeNumbers
	if ($UseParensForNegativeNumbers == -1) {
		$n_sign_posn = 0;
		if ($p_sign_posn == 0) {
			if (DEFAULT_P_SIGN_POSN != 0)
				$p_sign_posn = DEFAULT_P_SIGN_POSN;
			else
				$p_sign_posn = 3;
		}
	} elseif ($UseParensForNegativeNumbers == 0) {
		if ($n_sign_posn == 0)
			if (DEFAULT_P_SIGN_POSN != 0)
				$n_sign_posn = DEFAULT_P_SIGN_POSN;
			else
				$n_sign_posn = 3;
	}
	
	// check $GroupDigits
	if ($GroupDigits == -1) {
		$mon_thousands_sep = DEFAULT_MON_THOUSANDS_SEP;
	} elseif ($GroupDigits == 0) {
		$mon_thousands_sep = "";
	}

    // Start by formatting the unsigned number
    $number = number_format(abs($amount),
                            $frac_digits,
                            $mon_decimal_point,
                            $mon_thousands_sep);
							
	// check $IncludeLeadingDigit
	if ($IncludeLeadingDigit == 0) {
		if (substr($number, 0, 2) == "0.")
			$number = substr($number, 1, strlen($number)-1);		
	}
							
	if ($amount < 0) {
        $sign = $negative_sign;
        //The following statements "extracts" the boolean value as an integer 
        $n_cs_precedes  = intval($n_cs_precedes  == true);
        $n_sep_by_space = intval($n_sep_by_space == true);
        $key = $n_cs_precedes . $n_sep_by_space . $n_sign_posn;
    } else {
        $sign = $positive_sign;
        $p_cs_precedes  = intval($p_cs_precedes  == true);
        $p_sep_by_space = intval($p_sep_by_space == true);
        $key = $p_cs_precedes . $p_sep_by_space . $p_sign_posn;
    }
	
    $formats = array(
        // Currency symbol is after amount
        // No space between amount and sign.
        '000' => '(%s' . $currency_symbol . ')',
        '001' => $sign . '%s ' . $currency_symbol,
        '002' => '%s' . $currency_symbol . $sign,
        '003' => '%s' . $sign . $currency_symbol,
        '004' => '%s' . $sign . $currency_symbol,

        // One space between amount and sign.
        '010' => '(%s ' . $currency_symbol . ')',
        '011' => $sign . '%s ' . $currency_symbol,
        '012' => '%s ' . $currency_symbol . $sign,
        '013' => '%s ' . $sign . $currency_symbol,
        '014' => '%s ' . $sign . $currency_symbol,

        // Currency symbol is before amount
        // No space between amount and sign.
        '100' => '(' . $currency_symbol . '%s)',
        '101' => $sign . $currency_symbol . '%s',
        '102' => $currency_symbol . '%s' . $sign,
        '103' => $sign . $currency_symbol . '%s',
        '104' => $currency_symbol . $sign . '%s',

        // One space between amount and sign.
        '110' => '(' . $currency_symbol . ' %s)',
        '111' => $sign . $currency_symbol . ' %s',
        '112' => $currency_symbol . ' %s' . $sign,
        '113' => $sign . $currency_symbol . ' %s',
        '114' => $currency_symbol . ' ' . $sign . '%s');
		
    // We then lookup the key in the above array.
    return sprintf($formats[$key], $number);

}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------
// FormatNumber(Expression[,NumDigitsAfterDecimal [,IncludeLeadingDigit [,UseParensForNegativeNumbers [,GroupDigits]]]])
//
// NumDigitsAfterDecimal is the numeric value indicating how many places to the right of the decimal are displayed
// -1 Use Default
//
// The IncludeLeadingDigit, UseParensForNegativeNumbers, and GroupDigits arguments have the following settings:
// -1 True 
// 0 False 
// -2 Use Default

function FormatNumber($amount, $NumDigitsAfterDecimal, $IncludeLeadingDigit, $UseParensForNegativeNumbers, $GroupDigits) 
{
    //Export the values returned by localeconv into the local scope
    extract(localeconv());
	
	// Set defaults if locale is not set
	if (empty($currency_symbol)) $currency_symbol = DEFAULT_CURRENCY_SYMBOL;
	if (empty($mon_decimal_point)) $mon_decimal_point = DEFAULT_MON_DECIMAL_POINT;
	if (empty($mon_thousands_sep)) $mon_thousands_sep = DEFAULT_MON_THOUSANDS_SEP;
	if (empty($positive_sign)) $positive_sign = DEFAULT_POSITIVE_SIGN;
	if (empty($negative_sign)) $negative_sign = DEFAULT_NEGATIVE_SIGN;
	if ($frac_digits == CHAR_MAX) $frac_digits = DEFAULT_FRAC_DIGITS;
	if ($p_cs_precedes == CHAR_MAX) $p_cs_precedes = DEFAULT_P_CS_PRECEDES;
	if ($p_sep_by_space == CHAR_MAX) $p_sep_by_space = DEFAULT_P_SEP_BY_SPACE;
	if ($n_cs_precedes == CHAR_MAX) $n_cs_precedes = DEFAULT_N_CS_PRECEDES;
	if ($n_sep_by_space == CHAR_MAX) $n_sep_by_space = DEFAULT_N_SEP_BY_SPACE;
	if ($p_sign_posn == CHAR_MAX) $p_sign_posn = DEFAULT_P_SIGN_POSN;
	if ($n_sign_posn == CHAR_MAX) $n_sign_posn = DEFAULT_N_SIGN_POSN;
	
	// check $NumDigitsAfterDecimal
	if ($NumDigitsAfterDecimal > -1) 
		$frac_digits = $NumDigitsAfterDecimal;
	
	// check $UseParensForNegativeNumbers
	if ($UseParensForNegativeNumbers == -1) {
		$n_sign_posn = 0;
		if ($p_sign_posn == 0) {
			if (DEFAULT_P_SIGN_POSN != 0)
				$p_sign_posn = DEFAULT_P_SIGN_POSN;
			else
				$p_sign_posn = 3;
		}
	} elseif ($UseParensForNegativeNumbers == 0) {
		if ($n_sign_posn == 0)
			if (DEFAULT_P_SIGN_POSN != 0)
				$n_sign_posn = DEFAULT_P_SIGN_POSN;
			else
				$n_sign_posn = 3;
	}
	
	// check $GroupDigits
	if ($GroupDigits == -1) {
		$mon_thousands_sep = DEFAULT_MON_THOUSANDS_SEP;
	} elseif ($GroupDigits == 0) {
		$mon_thousands_sep = "";
	}

    // Start by formatting the unsigned number
    $number = number_format(abs($amount),
                            $frac_digits,
                            $mon_decimal_point,
                            $mon_thousands_sep);

	// check $IncludeLeadingDigit
	if ($IncludeLeadingDigit == 0) {
		if (substr($number, 0, 2) == "0.")
			$number = substr($number, 1, strlen($number)-1);
	}

	if ($amount < 0) {
        $sign = $negative_sign;
        $key = $n_sign_posn;
    } else {
        $sign = $positive_sign;
        $key = $p_sign_posn;
    }
	
    $formats = array(
        '0' => '(%s)',
        '1' => $sign . '%s',
		'2' => $sign . '%s',
		'3' => $sign . '%s',
		'4' => $sign . '%s');
		
    // We then lookup the key in the above array.
    return sprintf($formats[$key], $number);

}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------
// FormatPercent(Expression[,NumDigitsAfterDecimal [,IncludeLeadingDigit [,UseParensForNegativeNumbers [,GroupDigits]]]])
//
// NumDigitsAfterDecimal is the numeric value indicating how many places to the right of the decimal are displayed
// -1 Use Default
//
// The IncludeLeadingDigit, UseParensForNegativeNumbers, and GroupDigits arguments have the following settings:
// -1 True 
// 0 False 
// -2 Use Default

function FormatPercent($amount, $NumDigitsAfterDecimal, $IncludeLeadingDigit, $UseParensForNegativeNumbers, $GroupDigits) 
{
    //Export the values returned by localeconv into the local scope
    extract(localeconv());
	
	// Set defaults if locale is not set
	if (empty($currency_symbol)) $currency_symbol = DEFAULT_CURRENCY_SYMBOL;
	if (empty($mon_decimal_point)) $mon_decimal_point = DEFAULT_MON_DECIMAL_POINT;
	if (empty($mon_thousands_sep)) $mon_thousands_sep = DEFAULT_MON_THOUSANDS_SEP;
	if (empty($positive_sign)) $positive_sign = DEFAULT_POSITIVE_SIGN;
	if (empty($negative_sign)) $negative_sign = DEFAULT_NEGATIVE_SIGN;
	if ($frac_digits == CHAR_MAX) $frac_digits = DEFAULT_FRAC_DIGITS;
	if ($p_cs_precedes == CHAR_MAX) $p_cs_precedes = DEFAULT_P_CS_PRECEDES;
	if ($p_sep_by_space == CHAR_MAX) $p_sep_by_space = DEFAULT_P_SEP_BY_SPACE;
	if ($n_cs_precedes == CHAR_MAX) $n_cs_precedes = DEFAULT_N_CS_PRECEDES;
	if ($n_sep_by_space == CHAR_MAX) $n_sep_by_space = DEFAULT_N_SEP_BY_SPACE;
	if ($p_sign_posn == CHAR_MAX) $p_sign_posn = DEFAULT_P_SIGN_POSN;
	if ($n_sign_posn == CHAR_MAX) $n_sign_posn = DEFAULT_N_SIGN_POSN;
	
	// check $NumDigitsAfterDecimal
	if ($NumDigitsAfterDecimal > -1) 
		$frac_digits = $NumDigitsAfterDecimal;
	
	// check $UseParensForNegativeNumbers
	if ($UseParensForNegativeNumbers == -1) {
		$n_sign_posn = 0;
		if ($p_sign_posn == 0) {
			if (DEFAULT_P_SIGN_POSN != 0)
				$p_sign_posn = DEFAULT_P_SIGN_POSN;
			else
				$p_sign_posn = 3;
		}
	} elseif ($UseParensForNegativeNumbers == 0) {
		if ($n_sign_posn == 0)
			if (DEFAULT_P_SIGN_POSN != 0)
				$n_sign_posn = DEFAULT_P_SIGN_POSN;
			else
				$n_sign_posn = 3;
	}
	
	// check $GroupDigits
	if ($GroupDigits == -1) {
		$mon_thousands_sep = DEFAULT_MON_THOUSANDS_SEP;
	} elseif ($GroupDigits == 0) {
		$mon_thousands_sep = "";
	}

    // Start by formatting the unsigned number
    $number = number_format(abs($amount)*100,
                            $frac_digits,
                            $mon_decimal_point,
                            $mon_thousands_sep);
							
	// check $IncludeLeadingDigit
	if ($IncludeLeadingDigit == 0) {
		if (substr($number, 0, 2) == "0.")
			$number = substr($number, 1, strlen($number)-1);		
	}
							
	if ($amount < 0) {
        $sign = $negative_sign;
        $key = $n_sign_posn;
    } else {
        $sign = $positive_sign;
        $key = $p_sign_posn;
    }
	
    $formats = array(
        '0' => '(%s%%)',
        '1' => $sign . '%s%%',
		'2' => $sign . '%s%%',
		'3' => $sign . '%s%%',
		'4' => $sign . '%s%%');
		
    // We then lookup the key in the above array.
    return sprintf($formats[$key], $number);
}

function tempoRestante($dt1,$dt2){ 
    //Pega a data atual  Y-m-d H:i:s

    /* 
    * Faz a diferença entre a data do indicador e a data atual 
    * Retorna o resultado em segundos 
    */     
    $seg_restante = strtotime($dt1) - strtotime($dt2); 
        
    //Transforma o resultado em segundos para o formato H:i:s  
    if ($seg_restante<0){ 
		$seg_esgotado = abs($seg_restante); 
		$min_esgotado=floor($seg_esgotado/60); 
		$temp_esgotado=floor($min_esgotado/60).":".($min_esgotado%60).":".($seg_esgotado%60); 	
		$temp_restante=segundos($temp_esgotado); 
    } 
    else{ 
		$min_restante=floor($seg_restante/60); 
		$temp_restante=floor($min_restante/60).":".($min_restante%60).":".($seg_restante%60); 
    } 
	$tempo = explode(":", $temp_restante);
	
	if($tempo[0]<10)
		$tempo[0] = "0".$tempo[0];
	if($tempo[1]<10)
		$tempo[1] = "0".$tempo[1];
	if($tempo[2]<10)
		$tempo[2] = "0".$tempo[2];
    return implode(":",$tempo); 
} 

function somaHora ( $tempo1, $tempo2){ 
	$tempo1 = explode(":",$tempo1);
	$tempo2 = explode(":",$tempo2);

	$S = $tempo1[2] + $tempo2[2];
	if($S>=60){ $S = $S - 60; $M=1; } else { $M=0; }

	$M = $tempo1[1] + $tempo2[1] + $M;
	if($M>=60){ $M = $M - 60; $H=1; } else { $H=0; }

	$H = $tempo1[0] + $tempo2[0] + $H;
	if($H>=60) $H = $H - 60;

	if($H<=9){ $H="0".$H; }
	if($M<=9){ $M="0".$M; }
	if($S<=9){ $S="0".$S; }

	return $H.":".$M.":".$S;
}


//funcao pra formatar o tempo
function segundos($total){ //recebe total de segundos 
	$h=0; $m=0; $s=0; 

	list ($h,$m,$s) = explode(":",$total);
	if(strlen($h)==1) 
		$h = "0" . $h; 

	if(strlen($m)==1) 
		$m = "0" . $m; 
	
	if(strlen($s)==1)  
		$s = "0" . $s; 
	
	return $h . ":" . $m . ":" . $s; 
}

function datalocal() { 

		$mes_array = array("janeiro", "fevereiro", "março", "abril", "maio", "junho", "julho", "agosto", "setembro", "outubro", "novembro", "dezembro"); 
		$dia_atual= date('d'); 
		$mes_atual= gmdate('m'); 
		$ano_atual= gmdate('Y');  
		$extenso =  $dia_atual . " de " . $mes_array[$mes_atual-1] . " de " . $ano_atual; 
		
		return $extenso; 
	}
	
?>

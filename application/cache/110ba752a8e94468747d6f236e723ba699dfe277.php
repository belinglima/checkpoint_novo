<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo e(base_url('assets/css/materialize.min.css')); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo e(base_url('assets/css/styles.css')); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo e(base_url('assets/css/sweetalert.css')); ?>">
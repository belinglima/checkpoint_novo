<?php if(!!get_success_redirect()): ?>
    <div class="chip green darken-2">
        <span class="white-text"><?php echo get_success_redirect(); ?></span>
        <i class="close material-icons white-text left" style="margin-left: -8px; margin-right: 12px;">close</i>
    </div>
<?php elseif(!!get_error_redirect()): ?>
    <div class="chip red darken-2">
        <span class="white-text"><?php echo get_error_redirect(); ?></span>
        <i class="close material-icons white-text left" style="margin-left: -8px; margin-right: 12px;">close</i>
    </div>
<?php endif; ?>
 

<?php $__env->startSection('content'); ?>




	

	<div class="row full-width padding fundoroxo textoamarelo">
    	<div class="col s12 m8 left">
	        <h4 class="header">Check Point</h4>

	        <div class="left-align">
	        	Última Atualização <b> <?php echo e(date("H:i:s - d/m/Y")); ?></b>
	    	</div>
	    </div>

    	<div class="col s12 m2 right" style="margin-top: 20px;">

		    <a class="btn fundoamarelo textoroxo col s12 m4" style="margin-right: 5px; margin-bottom: 5px;" href="<?php echo e(base_url('login/logout')); ?>">
		    	
				    <i class="material-icons">logout</i>
				
			</a>

			<div class="btn fundoverde col s12 m4 botaoAlerta modal-trigger" href="#modalAlerta">
		    	
				    <i class="material-icons">warning</i>
				
			</div>

		</div>
	</div>

	 <div class="row center-align textoroxo topo">
    	<div class="col s6 m2 borda fundoverde tamanhopadrao paddingtopo bold">
    		227
    	</div>
    	<div class="col s6 m2 borda fundoverde tamanhopadrao paddingtopo bold">
    		240
    	</div>
    	<div class="col s6 m2 borda fundoverde tamanhopadrao paddingtopo bold">
    		243
    	</div>
    	<div class="col s6 m2 borda fundoverde tamanhopadrao paddingtopo bold">
    		245
    	</div>
    	<div class="col s6 m2 borda fundoverde tamanhopadrao paddingtopo bold">
    		250
    	</div>
    	<div class="col s6 m2 borda fundoverde tamanhopadrao paddingtopo bold">
    		OI PRIMARIO
    	</div>
    </div>

    <div class="row center-align textoroxo topo">
    	
    	<div class="col s12 m4 fundoverde bold fim borda">
    		<p class="col s12 fundoroxo textoamarelo full-width borda">Arena</p>
    		
    		<div class="col s6 borda">
    			<p class="col s12 fundoroxo textoamarelo full-width topoinside borda">Claro</p>

    			<p class="textopequeno">BANCO 1	01/01/1970 00:00:00h</p>
    			<p class="textopequeno">BANCO 2	01/01/1970 00:00:00h</p>
    			<p class="textopequeno">BANCO 3	01/01/1970 00:00:00h</p>
    		</div>
    		<div class="col s6 borda">
    			<p class="col s12 fundoroxo textoamarelo full-width topoinside borda">Tim</p>

    			<p class="textopequeno">BANCO 1	01/01/1970 00:00:00h</p>
    			<p class="textopequeno">BANCO 2	01/01/1970 00:00:00h</p>
    			<p class="textopequeno">BANCO 3	01/01/1970 00:00:00h</p>
    		</div>
    	</div>

    	<div class="col s12 m4 fundoverde bold fim2 borda">
    		<p class="col s12 fundoroxo textoamarelo full-width borda">Arena v2</p>
    		
    		<div class="col s6 borda">
    			<p class="col s12 fundoroxo textoamarelo full-width topoinside borda">GPRS</p>

    			<p class="textopequeno">15/08/2021 21:19:14h</p>
    		</div>
    		<div class="col s6 borda">
    			<p class="col s12 fundoroxo textoamarelo full-width topoinside borda">Satelital</p>

    			<p class="textopequeno">15/08/2021 21:19:02h</p>
    		</div>
    	</div>

    	<div class="col s12 m4 fundoverde bold fim2 borda">
    		<p class="col s12 fundoroxo textoamarelo full-width borda">Suntech</p>
    		
    		<div class="col s12 borda">
    			<p class="col s12 fundoroxo textoamarelo full-width topoinside borda">Gateway</p>

    			<p class="textopequeno">01/01/1970 00:00:00h</p>
    		</div>
    	</div>

    </div>

     <div class="row center-align textoroxo topo">
     	<div class="col s12 fundoverde bold fim borda">
    		<p class="col s12 fundoroxo textoamarelo full-width borda">Número de Conexões</p>

    		<div class="col s12 m3 borda">
    			<p class="col s12 fundoroxo textoamarelo full-width topoinside borda">MIRROR</p>

    			<p class="textopequeno padding">01</p>
    		</div>

    		<div class="col s12 m3 borda">
    			<p class="col s12 fundoroxo textoamarelo full-width topoinside borda">ARENA 1</p>

    			<p class="textopequeno padding">01</p>
    		</div>

    		<div class="col s12 m3 borda">
    			<p class="col s12 fundoroxo textoamarelo full-width topoinside borda">ARENA 2</p>

    			<p class="textopequeno padding">01</p>
    		</div>

    		<div class="col s12 m3 borda">
    			<p class="col s12 fundoroxo textoamarelo full-width topoinside borda">ARENA 3</p>

    			<p class="textopequeno padding">01</p>
    		</div>
    	</div>
     </div>

	
			
		

		


		

			

		

		
	



  <!-- Modal Structure -->
  <div id="modalAlerta" class="modal">
    <div class="modal-content fundoroxo textoamarelo full-width center-align">
      <h4>Alerta</h4>
      <h5>Deseja ativar o alerta nos portais?</h5>
      <p>Ao ativar o alerta, os portais de rastreamento ficam avisando <br> para seus usuarios que estamos tendo problemas técnicos!</p>

      <a href="#!" class="btn  fundoverde textoroxo bold borda"> Ativar</a> <a
      href="#!" class="btn  fundoverde textoroxo bold borda">
      Desativar</a> </div> </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('extra-javascript'); ?>
	<script type="text/javascript">
	 //    document.addEventListener('DOMContentLoaded', function() {
		//     var elems = document.querySelectorAll('.fixed-action-btn');
		//     var instances = M.FloatingActionButton.init(elems, {
		//       toolbarEnabled: true
		//     });
		// });
		$(document).ready(function(){
	    	$('.modal').modal();
	  	});
		
	</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('template.base', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
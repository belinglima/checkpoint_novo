<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>.:: CheckPoint ::.</title>
	<link rel="shortcut icon" 
  		href="<?php echo e(base_url('assets/images/favicon.ico')); ?>" type="image/x-icon">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="<?php echo e(base_url('assets/css/materialize.min.css')); ?>" rel="stylesheet" media="screen,projection"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<style>
	    body {
	      background: #eee;
	    }
	    .full-width {
	    	border-radius: 8px;
	    	width: 100%;
	    }
  	</style>
</head>
<body onLoad="document.logar.login.focus();">
<main>
    <div class="container">
    	<div class="row" style="margin-top: 20px;">
    		<div class="col s12 m4"></div>
    		<div class="col s12 m4">
		        <form name="logar" action="<?php echo e(base_url('login')); ?>" method="POST">
		        	<div class="card">
		        		<div class="card-content">
		        			<div class="col s12 center-align">
			        			<div class="btn-floating btn-large">
				        			<i class="material-icons medium green">flag</i>
				        		</div>
				        	</div>
		              		<div class='input-field col s12'>
		                		<input class='validate' type='text' name='login' id='login' />
		                		<label for='login'>Usuário</label>
		              		</div>
		              		<div class='input-field col s12'>
		                		<input class='validate' type='password' name='senha' id='senha' />
		                		<label for='senha'>Senha</label>
		              		</div>
			            	<div id="entrar" class="btn-large indigo white-text full-width">
			            		Entrar
			            	</div>
			            </div>
		      		</div>
			    </form>
			</div>
		    <div class="col s12 m4"></div>
		</div>
	</div>
</main>

<script src="<?php echo e(base_url('assets/javascript/jquery-3.3.1.min.js')); ?>"></script>
<script src="<?php echo e(base_url('assets/javascript/materialize.min.js')); ?>"></script>
	
<script type="text/javascript">
	function submitForm() {
		if ($("#login").val() == "") {
			M.toast({html: 'Insira um login!'});
		}
		if ($("#senha").val() == "") {
			M.toast({html: 'Insira uma senha!'});
		}
		if( $("#login").val() != "" &&
			$("#senha").val() != "") {
			document.logar.submit();
		}
	};

	$("body").keypress(function(event){
		if( event.which == 13 ){
			event.preventDefault();
			submitForm();
		}
	}); 

	$("#entrar").click(function(){
		submitForm();
	});

	<?php if(isset($msg) && !empty($msg)): ?>
		$(document).ready(function(){
			M.toast({html: '<?php echo e($msg); ?>'});
		});
	<?php endif; ?>
</script>
</body>
</html>
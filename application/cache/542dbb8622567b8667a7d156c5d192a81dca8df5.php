<div class="alerta-topo">
    <?php echo $__env->make('template.show-error-success-redirect', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</div>

<div class="parallax-container indigo darken-2">
    <div class="container">
        <h3 class="header white-text">Check Point</h3>

        <div class="left-align" style="margin-top: -15px; color: white">
        	Última Atualização <b> <?php echo e(date("H:i:s - d/m/Y")); ?></b>
    	</div>

	    <a href="<?php echo e(base_url('login/logout')); ?>">
	    	<div class="right btn red darken-2" style="margin-top: -65px;">
			    Sair
			    <i class="material-icons right">chevron_right</i>
			</div>
		</a>
    </div>
</div>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>.:: CheckPoint ::.</title>
	<link rel="shortcut icon" 
  		href="<?php echo e(base_url('assets/images/favicon.ico')); ?>" type="image/x-icon">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="<?php echo e(base_url('assets/css/materialize.min.css')); ?>" rel="stylesheet" media="screen,projection"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<style>
	    body {
	      background: #eee;
	      padding: 10px;
	    }
	    .full-width {
	    	border-radius: 8px;
	    	width: 100%;
	    }

	    .card .card-content {
		    padding: 14px !important;
		}

		.row .col {
		    float: left;
		    -webkit-box-sizing: border-box;
		    box-sizing: border-box;
		     padding:  0.0rem; 
		    min-height: 1px;
		}

		#toast-container .toast {
		    background: #792357 !important;
		    color: #efb824 !important;
		    border-radius: 10px;
		    border: 1px dashed #efb824;
		    font-weight: 900;
		}


	    input {
		  border: 0px;
		  padding: 12px 10px;
		  border-radius: 5px;
		  -moz-border-radius: 5px;
		  -webkit-border-radius: 5px;
		  -o-border-radius: 5px;
		  -ms-border-radius: 5px;
		  font-size: 18px;
		  font-family: 'Poppins'; 
		  outline: none;
		  background: #efb824;
		}

		label, input {
		  display: block;
		  width: 100%; 
		  color: #792357;
		}

		.input-field p {
			color: #efb824;
			font-weight: 900;
			font-size: 18px;
		}
  	</style>
</head>
<body onLoad="document.logar.login.focus();" 
	style="
		background: url(../assets/images/bg-01.png) no-repeat center center fixed; 
		-webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
		background-size: cover;
	">
<main>
    <div class="container">
    	<div class="row">
    		<div class="col s12 m4"></div>

    		<div class="col s12 m4" 
    			style="
    				position: absolute; 
    				top: 50%; 
    				left: 50%; 
    				transform: translate(-50%, -50%);
    			">
		        <form name="logar" action="<?php echo e(base_url('login')); ?>" method="POST">
		        	<div class="card" style="border-radius: 10px; background: #792357; border: 1px dashed #efb824; margin: 20px;">
		        		<div class="card-content">
		        			<div class="col s12 center-align" 
		        				style="
		        				    display: block;
								    font-family: Poppins-Bold;
								    font-size: 39px;
								    color: #efb824;
								    line-height: 1.2;
								    text-align: center;
								    font-weight: 900; 
		        				">
			        			<span>Checkpoint</span>
				        	</div>
		              		<div class='input-field col s12'>
		                		<p>Usuário</p>
		                		<input class='validate browser-default' type='text' name='login' id='login' />
		              		</div>
		              		<div class='input-field col s12' style="margin-top: -10px; margin-bottom: 40px;">
		              			<p>Senha</p>
		                		<input class='validate browser-default' type='password' name='senha' id='senha' />
		              		</div>
			            	<div id="entrar" class="btn-large full-width" 
			            		style="
			            			background: #efb824; 
			            			color: #792357; 
			            			font-weight: 900; 
			            			font-size: 18px;
			            		">
			            		Conectar
			            	</div>
			            </div>
		      		</div>
			    </form>
			</div>

		    <div class="col s12 m4"></div>
		</div>
	</div>
</main>

<script src="<?php echo e(base_url('assets/javascript/jquery-3.3.1.min.js')); ?>"></script>
<script src="<?php echo e(base_url('assets/javascript/materialize.min.js')); ?>"></script>
	
<script type="text/javascript">
	function submitForm() {
		if ($("#login").val() == "") {
			M.toast({html: 'Insira um login!'});
		}
		if ($("#senha").val() == "") {
			M.toast({html: 'Insira uma senha!'});
		}
		if( $("#login").val() != "" &&
			$("#senha").val() != "") {
			document.logar.submit();
		}
	};

	$("body").keypress(function(event){
		if( event.which == 13 ){
			event.preventDefault();
			submitForm();
		}
	}); 

	$("#entrar").click(function(){
		submitForm();
	});

	<?php if(isset($msg) && !empty($msg)): ?>
		$(document).ready(function(){
			M.toast({html: '<?php echo e($msg); ?>'});
		});
	<?php endif; ?>
</script>
</body>
</html>
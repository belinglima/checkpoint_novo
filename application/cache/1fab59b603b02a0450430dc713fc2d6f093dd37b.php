<script type="text/javascript" src="<?php echo e(base_url('assets/javascript/jquery-3.3.1.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(base_url('assets/javascript/materialize.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(base_url('assets/javascript/jquery.mask.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(base_url('assets/javascript/sweetalert.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(base_url('assets/javascript/functions.js?v='.Version)); ?>"></script>


<?php echo $__env->yieldContent('extra-javascript'); ?>
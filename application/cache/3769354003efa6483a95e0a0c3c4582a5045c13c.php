<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>.:: CheckPoint ::.</title>
	<meta http-equiv="refresh" content="30;url=welcome" />
	<link rel="shortcut icon" 
		href="<?php echo e(base_url('assets/images/favicon.ico')); ?>" type="image/x-icon">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="theme-color" content="#1a237e">
	<?php echo $__env->make('template.css', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</head>
<body>
	<main>
		<?php echo $__env->make('template.menu-topo', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php echo $__env->yieldContent('content'); ?>
	</main>
</body>
<script type="text/javascript">
	function base_url(caminho) {
		return '<?php echo e(base_url()); ?>' + caminho;
	}
</script>
<?php echo $__env->make('template.javascript', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</html>
 

<?php $__env->startSection('content'); ?>

<?php 
	$str = array('<second>', '</second>');

	$bancoArena1 = $conexoes ? str_replace($str, "", $conexoes->ARENA_1) : false;
	$bancoArena2 = $conexoes ? str_replace($str, "", $conexoes->ARENA_2) : false;
	$bancoArena3 = $conexoes ? str_replace($str, "", $conexoes->ARENA_3) : false;

	if (!$bancoArena1){
		$bancoArena1 = '0';
	}
	
	if (!$bancoArena2){
		$bancoArena2 = '0';
	}
	
	if (!$bancoArena3){
		$bancoArena3 = '0';
	}

	if ($conexoes) {
		$mirrorData = ($conexoes->MIRROR_0 > 0)  ? $conexoes->MIRROR_0 : 0;
	} else {
		$mirrorData = 0;
	}

	$bancosTim = [
		1 => '',
		2 => '',
		3 => ''
	];

	$SYNC_TIMMMM = 0;
	$SYNC_CLAROOOO = 0;

	$ontem = date('Y-m-d H:i:s', strtotime('-15 minutes'));

	for ($i = 1; $i <=3 ; $i++) :
		if ( $dados['banco_'.$i.'_tim'] < $ontem) :
			$SYNC_TIMMMM = '1';
			$bancosTim[$i] = "red lighten-4";
		endif;
	endfor;

	$bancosClaro = [
		1 => '',
		2 => '',
		3 => ''
	];

	for ($i = 1; $i <=3 ; $i++) :
		if ( $dados['banco_'.$i.'_claro'] < $ontem) :
			$SYNC_CLAROOOO = '1';
			$bancosClaro[$i] = "red lighten-4";
		endif;
	endfor;

	function ping($host) {
        exec(sprintf('ping -c 1 -W 5 %s', escapeshellarg($host)), $res, $rval);
        return $rval === 0;
	}

	$arrayIps = array();
	$color = "green";
	$alert = "";
	$names = array('OI_PRIMÁRIO', '227', '240', '245', '250');
	$host = array('200.102.219.67', '186.215.192.227', '186.215.192.240', '186.215.192.245', '186.215.192.250');

	for ($j = 0; $j < count($host); $j++) :
		$ips = new stdClass;
		if( ping( $host[$j] ) ) {
			$ips->name = $names[$j];
			$ips->status = ping($host[$j]);
			$ips->cor = "green";
			$ips->alerta = '0';
		} else {
			$ips->name = $names[$j];
			$ips->status = ping($host[$j]);
			$ips->cor = "red darken 2";
			$color = "red darken 2";
			$alert = "pulse";
			$ips->alerta = '1';
		}
		$arrayIps[$j] = $ips;
	endfor;
 ?>

<div class="container">
	<div class="hide">
		<audio id="audio" controls>
		   <source src="<?php echo e(base_url('assets/songs/ALERTA.mp3')); ?>" type="audio/mp3" />
		</audio>
	</div>

	<div class="row">

		<?php if(!$mirrorData): ?>
			<div class="col s12" style="border-radius: 12px; margin-top: 10px; margin-bottom: 20px;">
				<div class="card red darken-2 white-text">
					<div class="card-content center-align">
						<h5>
							IIS sem comunicação necessário reiniciar.
						</h5>
						<a href="<?php echo e($data['ERRO']); ?>" target="_blank">ERRO 500</a>
					</div>
				</div>
			</div>
		<?php endif; ?>


		<div class="col s6">

			<div class="card green lighten-3 sucesso-arena" style="border-radius: 12px; margin-top: -14px;">
				<div class="card-content center-align">
					<i class="material-icons large green-text text-darken-4">done</i>
					<h5>ARENA</h5>
				</div>
			</div>	

			<div class="card red darken-2 deu-erro-arena hide" style="border-radius: 12px; margin-top: -14px;">
				<div class="card-content center-align">
					<i class="material-icons large white-text">warning</i>
					<h5 class="white-text">ARENA</h5>
					<p class="white-text">
						<b>
							<?php echo e($SYNC_CLAROOOO == 1 ? 'SOMENTE ALERTA SEM DISPARO' : ''); ?>

						</b>
					</p>
				</div>
			</div>

			<div class="card" style="margin-top: 10px;">
				<div class="card-content center-align">
					<table>
						<tbody>
							<tr>
								<td colspan="2"><b><?php echo e($dados['claro']); ?></b></td>
							</tr>
							<tr class="<?php echo e($bancosClaro[1]); ?>">
								<td><b>BANCO 1</b></td>
								<td><?php echo e(format_datetime_to_show($dados['banco_1_claro'])); ?>h</td>
							</tr>
							<tr class="<?php echo e($bancosClaro[2]); ?>">
								<td><b>BANCO 2</b></td>
								<td><?php echo e(format_datetime_to_show($dados['banco_2_claro'])); ?>h</td>
							</tr>
							<tr class="<?php echo e($bancosClaro[3]); ?>">
								<td><b>BANCO 3</b></td>
								<td><?php echo e(format_datetime_to_show($dados['banco_3_claro'])); ?>h</td>
							</tr>
						</tbody>
					</table>
					<p>
						<b>
							<?php echo e($dados['sinc_Claro'] == 1 ? '*** Dados dessincronizados ***' : ''); ?>

						</b>
					</p>
					<p>
						<b>
							<?php echo e($SYNC_CLAROOOO == 1 ? '*** Com erro a mais de 15 minutos ***' : ''); ?>

						</b>
					</p>
				</div>
			</div>

			<div class="card" style="margin-top: 10px;">
				<div class="card-content center-align">
					<table>
						<tbody>
							<tr>
								<td colspan="2"><b><?php echo e($dados['tim']); ?></b></td>
							</tr>
							<tr class="<?php echo e($bancosTim[1]); ?>">
								<td><b>BANCO 1</b></td>
								<td><?php echo e(format_datetime_to_show($dados['banco_1_tim'])); ?>h</td>
							</tr>
							<tr class="<?php echo e($bancosTim[2]); ?>">
								<td><b>BANCO 2</b></td>
								<td><?php echo e(format_datetime_to_show($dados['banco_2_tim'])); ?>h</td>
							</tr>
							<tr class="<?php echo e($bancosTim[3]); ?>">
								<td><b>BANCO 3</b></td>
								<td><?php echo e(format_datetime_to_show($dados['banco_3_tim'])); ?>h</td>
							</tr>
						</tbody>
					</table>
					<p></p>
					<p>
						<b>
							<?php echo e($dados['sinc_Tim'] == 1 ? '*** Dados dessincronizados ***' : ''); ?>

						</b>
					</p>
					<p>
						<b>
							<?php echo e($SYNC_TIMMMM == 1 ? '*** Com erro a mais de 15 minutos ***' : ''); ?>

						</b>
					</p>
				</div>
			</div>
		</div>

		<div class="col s6">

			<div class="card green lighten-3 sucesso-am" style="border-radius: 12px; margin-top: -14px;">
				<div class="card-content center-align">
					<i class="material-icons large green-text text-darken-4">done</i>
					<h5>AM</h5>
				</div>
			</div>	

			<div class="card red darken-2 deu-erro-am hide" style="border-radius: 12px; margin-top: -14px;">
				<div class="card-content center-align">
					<i class="material-icons large white-text">warning</i>
					<h5 class="white-text">AM</h5>
					<p class="white-text">
						<b>
							<?php echo e($SYNC_CLAROOOO == 1 ? 'SOMENTE ALERTA SEM DISPARO' : ''); ?>

						</b>
					</p>
				</div>
			</div>

			<div class="card" style="margin-top: 10px;">
				<div class="card-content center-align">
					<table>
						<tbody>
							<tr>
								<td><b>GPRS</b></td>
								<td><?php echo e(format_datetime_to_show($dados['ArenaV2_gprs'])); ?>h</td>
							</tr>
							<tr>
								<td><b>SATELITAL</b></td>
								<td><?php echo e(format_datetime_to_show($dados['ArenaV2_satelital'])); ?>h</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

			<div class="card <?php echo e($mirrorData == 0 ? 'red darken-2 white-text' : ''); ?>" style="margin-top: 10px;">
				<div class="card-content center-align">
					<b>Número de Conexões</b>
					<table>
						<tbody>
							<tr>
								<td><b>MIRROR</b></td>
								<td colspan="2"><?php echo e($mirrorData); ?></td>
							</tr>
							<tr>
								<td><b>BANCO ARENA 1</b></td>
								<td><?php echo e(($bancoArena1 !== null) ? $bancoArena1 : "PARADO"); ?></td>
							</tr>
							<tr>
								<td><b>BANCO ARENA 2</b></td>
								<td><?php echo e(($bancoArena2 !== null) ? $bancoArena2 : "PARADO"); ?></td>
							</tr>
							<tr>
								<td><b>BANCO ARENA 3</b></td>
								<td><?php echo e(($bancoArena3 !== null) ? $bancoArena3 : "PARADO"); ?></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

		</div>

		<div class="fixed-action-btn toolbar direction-top">
	      	<a class="btn-floating btn-large <?php echo e($color); ?> <?php echo e($alert); ?> ">
	        	<i class="large material-icons">iso</i>
	      	</a>
	      	<ul>
	      		<?php $__currentLoopData = $arrayIps; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			        <li class="<?php echo e($arrayIps[$key]->cor); ?>">
			        	<a class="waves-effect waves-light btn"><?php echo e($arrayIps[$key]->name); ?></a>
			        </li>

			       	<?php 
						if($arrayIps[$key]->alerta == '1'){
							echo "<script> document.addEventListener('DOMContentLoaded', function(){ audio.play(); }); </script>";
							break;
						}
					 ?>
	      		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	      	</ul>
	    </div>

	</div>

</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('extra-javascript'); ?>
	<script type="text/javascript">
	    document.addEventListener('DOMContentLoaded', function() {
		    var elems = document.querySelectorAll('.fixed-action-btn');
		    var instances = M.FloatingActionButton.init(elems, {
		      toolbarEnabled: true
		    });
		});

		let CHECKPOINT = '<?php echo e($data['CHECKPOINT']); ?>';
		let ARENAV2_GPRS = '<?php echo e($data['ARENAV2_GPRS']); ?>';
		let ARENAV2_SATELITAL = '<?php echo e($data['ARENAV2_SATELITAL']); ?>';
		let MIRROR = '<?php echo e($data['MIRROR']); ?>';
		let ARENA = '<?php echo e($data['ARENA']); ?>';
		let SYNC_CLARO = '<?php echo e($dados['sinc_Claro']); ?>';
		let SYNC_TIM = '<?php echo e($dados['sinc_Tim']); ?>';
		let ALERTA_GPRS = '<?php echo e($dados['alertaGPRS']); ?>';
		let CON_ARENA_1 = '<?php echo e($bancoArena1); ?>';
		let CON_ARENA_2 = '<?php echo e($bancoArena2); ?>';
		let CON_ARENA_3 = '<?php echo e($bancoArena3); ?>';
		let CON_MIRROR = '<?php echo e($mirrorData); ?>';
		var TIM = '<?php echo e($SYNC_TIMMMM); ?>';
		var CLARO = '<?php echo e($SYNC_CLAROOOO); ?>';
		var ERRO = '<?php echo e($data['ERRO']); ?>';
		var URL = '<?php echo e($endereco); ?>';

		let dados = {
			'SYNC_TIM': SYNC_TIM,
			'SYNC_CLARO': SYNC_CLARO,
			'CHECKPOINT': CHECKPOINT,
			'MIRROR': MIRROR,
			'ARENA': ARENA,
			'ARENAV2_GPRS': ARENAV2_GPRS,
			'ARENAV2_SATELITAL': ARENAV2_SATELITAL,
			'ALERTA_GPRS': ALERTA_GPRS
		};

		console.log(ERRO)

		console.table(dados);

		let dados1 = {
			'CON_ARENA_1': CON_ARENA_1,
			'CON_ARENA_2': CON_ARENA_2,
			'CON_ARENA_3': CON_ARENA_3,
			'CON_MIRROR': CON_MIRROR,
			'URL':URL
		};

		console.table(dados1);

		let dados2 = {
			'CON_ARENA_0[2] - ERRO TIM': TIM,
			'CON_ARENA_0[0] - ERRO CLARO': CLARO
		};
		
		console.table(dados2);

		const chamaErroArena = (resposta) => {
			if (TIM == '1' && CLARO == '1') {
				$(".sucesso-arena").toggleClass("hide");
				$(".deu-erro-arena").toggleClass("hide");
			} 
			if (resposta) {
				audio.play();
				$(".sucesso-arena").toggleClass("hide");
				$(".deu-erro-arena").toggleClass("hide");
			}
        }

		const chamaErroAM = () => {
			audio.play();
			$(".sucesso-am").toggleClass("hide");
			$(".deu-erro-am").toggleClass("hide");
		}

	  	var audio = document.getElementById('audio');
	  	document.getElementById('audio').muted = false;

		if (CHECKPOINT == '1' || 
			MIRROR == '1' || 
			ARENA == '1' ||
			CON_ARENA_1 == '0' || 
			CON_ARENA_2 == '0' || 
			CON_ARENA_3 == '0'
		) {
			chamaErroArena(true);
		} else {
			chamaErroArena(false);
		}

		if (ARENAV2_GPRS == '1' || 
			ARENAV2_SATELITAL == '1' || 
			ALERTA_GPRS == '1'
		) {
			chamaErroAM();
		}
	</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('template.base', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
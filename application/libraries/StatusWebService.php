<?php

class StatusWebService {
    
    const ARENA_1 = "http://186.215.192.245/scripts13/arenawebservice.dll/";
    const ARENA_2 = "http://186.215.192.245/scripts13_2/arenawebservice.dll/";
    const ARENA_3 = "http://186.215.192.245/scripts13_3/arenawebservice.dll/";
    const DADOSARENA = "http://186.215.192.245/asfd09ds8f7dsa0sd897sad09f8d7.php";

    static function getStatusArena($status) {
        
        if (! array_key_exists($status, self::$status)) {
            throw new Exception("Endereco não existe");
        }

        return self::$status;
    }
}


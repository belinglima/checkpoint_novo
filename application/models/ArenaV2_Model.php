<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ArenaV2_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function consultaSATELITAL()
    {
		$this->db->select('max(SYSTEM_DATA) as DATA')
            ->from('ultimo_dado_lido AS a')
            ->where('a.TIPO_POSICAO', 'SATELITAL');

        $response = $this->db->get()->row();

        return $response;
    }

    public function consultaGRPS()
    {
    	$this->db->select('max(SYSTEM_DATA) as DATA')
            ->from('ultimo_dado_lido AS a')
            ->where('a.TIPO_POSICAO', 'GPRS');

        $response = $this->db->get()->row();

        return $response;
    }

}
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Arena_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('StatusWebService');
        $this->load->model('ArenaV2_Model');
        date_default_timezone_set('America/Sao_Paulo');
    }

    public function cabecalho()
    {
        $placa = "CHECKPOINT";

        $XML  = '<FILTER>';
        $XML .= "<DESC>$placa</DESC>";
        $XML .= "<LIMIT>";
        $XML .= "<QUANT>10</QUANT>";
        $XML .= "<OFFSET>0</OFFSET>";
        $XML .= "</LIMIT>";
        $XML .= "<SHOW_FIELDS>";
        $XML .= "<ID>1</ID>";
        $XML .= "<DESC>21</DESC>";
        $XML .= "<TYPE_DESC>1</TYPE_DESC>";
        $XML .= "<NEAR_DESC>1</NEAR_DESC>";
        $XML .= "<DATE_TIME>1</DATE_TIME>";
        $XML .= "<OUT1>1</OUT1>";
        $XML .= "<OUT2>1</OUT2>";
        $XML .= "<OUT3>1</OUT3>";
        $XML .= "<OUT4>1</OUT4>";
        $XML .= "</SHOW_FIELDS>";
        $XML .= "</FILTER>";

        return $XML;
    }

    //#####################################   BANCO 1    ####################################
    public function arena_0()
    {
        $XML = $this->cabecalho();

        $Texto1 = "";
        $cont = 0;
        $Erro1 = "";

        while ($cont < 4 && $Texto1 == "" && $Erro1 == "") {
            $Response = '';
            $opts = array('timeout' => 10);
            $ReqVeiculos = new HTTP_Request(get_webservice_0()."maplastdataread?WebBrokerSessionID=".get_userID_arena_0(), $opts);
            $ReqVeiculos->setMethod(HTTP_REQUEST_METHOD_POST);
            $ReqVeiculos->clearPostData();
            $ReqVeiculos->addRawPostData($XML,True);
            $ReqVeiculos->sendRequest();
            // $ReqVeiculos->setTimeout(1);
            $Response = $ReqVeiculos->getResponseBody();
            $cont++;

            $code = $this->statusCode(get_webservice_0());
            if (! $code) {
                return false;
            }

            // verifica se deu algum problema na consulta
            $Erro1 = verificaErro($Response);
            if (!$Erro1) {
                return readXML($Response,"VEHICLES");
            }

            return false;
        }
    }

    //#####################################   BANCO 2    ####################################
    public function arena_1()
    {
        $XML = $this->cabecalho();

        $Response = '';
        $opts = array('timeout' => 10);
        $ReqVeiculos = new HTTP_Request(get_webservice_1()."maplastdataread?WebBrokerSessionID=".get_userID_arena_1(), $opts);
        //echo "<br>ID DA SESSAO DO BANCO2: ".$IDSessaoArena_2;
        $ReqVeiculos->setMethod(HTTP_REQUEST_METHOD_POST);
        $ReqVeiculos->clearPostData();
        $ReqVeiculos->addRawPostData($XML,True);
        $ReqVeiculos->sendRequest();
        $Response = $ReqVeiculos->getResponseBody();

        $code = $this->statusCode(get_webservice_1());
        if (! $code) {
            return false;
        }
        // verifica se deu algum problema
        $Erro2 = verificaErro($Response);
        if (!$Erro2){
            return readXML($Response,"VEHICLES");
        } 

        return false;
    }

    //#####################################   BANCO 3    ####################################
    public function arena_2()
    {
        $XML = $this->cabecalho();

        $Response = '';
        $opts = array('timeout' => 10);
        $ReqVeiculos = new HTTP_Request(get_webservice_2()."maplastdataread?WebBrokerSessionID=".get_userID_arena_2(), $opts);
        //echo "<br>ID DA SESSAO DO BANCO3: ".$IDSessaoArena_3;
        $ReqVeiculos->setMethod(HTTP_REQUEST_METHOD_POST);
        $ReqVeiculos->clearPostData();
        $ReqVeiculos->addRawPostData($XML,True);
        $ReqVeiculos->sendRequest();
        $Response = $ReqVeiculos->getResponseBody(get_webservice_2());

        $code = $this->statusCode(get_webservice_2());
        if (! $code) {
            echo $Response;
            echo "<br>";

            return false;
        }

        // verifica se deu algum problema
        $Erro3 = verificaErro($Response);
        if (!$Erro3){
            return readXML($Response,"VEHICLES");
        }

        echo $Response;
        echo "<br>";
        return false;
    }

    public function statusCode($webservice)
    {
        $ch = curl_init();
        $timeout = 10;
        // curl_setopt($ch, CURLOPT_HEADER, true);

        curl_setopt($ch, CURLOPT_URL, $webservice);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $conteudo = curl_exec ($ch);
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        
        if ($http_status == '200') {
            return true;   
        }

        return false;
    }

    // DADOSARENA consulta erro no 245 yes
    public function consultaNumerosConexao()
    {
        $ARENA = StatusWebService::DADOSARENA;
        $ch = curl_init();
        $timeout = 1;
        // curl_setopt($ch, CURLOPT_HEADER, true);

        curl_setopt($ch, CURLOPT_URL, $ARENA);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $conteudo = curl_exec ($ch);
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        
        if ($http_status == '200') {
            return json_decode($conteudo);    
        }
        
        return false;
    }

    public function consultaQntXml()
    {
        $end = 'http://186.215.192.240/qnt_xml.php';
        $ch = curl_init();
        $timeout = 10;
        // curl_setopt($ch, CURLOPT_HEADER, true);

        curl_setopt($ch, CURLOPT_URL, $end);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $conteudo = curl_exec ($ch);
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $data = json_decode($conteudo);

        if ($http_status == '200') {
            return $data->qnt;   
        }

        return false;
    }

    public function logica()
    {
        $data2 = $this->arena_2();
        $numConexao = $this->consultaNumerosConexao();

        // echo "<pre>";
        // var_dump($numConexao);
        // die;

        $arenav2SATELITAL = $this->ArenaV2_Model->consultaSATELITAL();
        $arenav2GPRS = $this->ArenaV2_Model->consultaGRPS();

        $tempototalserv = ((date("H")+1) * 60) + date("i");
        $tempototalserv = strtotime("now");

        if($data2){
	        $tempA = explode(" ",$data2[0]['DATE_TIME']);
	        $horaA = explode(":",$tempA[1]);
	        $tempototalmoduloA = (($horaA[0]+1) * 60) + $horaA[1];
	        $tempototalmoduloA = strtotime($data2[0]['DATE_TIME']);

	        $tempB = explode(" ",$data2[2]['DATE_TIME']);
	        $horaB = explode(":",$tempB[1]);
	        $tempototalmoduloB = (($horaB[0]+1) * 60) + $horaB[1];  
	        $tempototalmoduloB = strtotime($data2[2]['DATE_TIME']);
	        
	        // CALCULO DO TEMPO DOS MODULOS.
	        $CHECKPOINT_CLARO = ($tempototalserv - $tempototalmoduloA);
	        $CHECKPOINT_TIM = ($tempototalserv - $tempototalmoduloB);
        } else {
        	$CHECKPOINT_CLARO = 1000;
        	$CHECKPOINT_TIM = 1000;
        }

        $tempC = explode(" ",$arenav2GPRS->DATA);
        $horaC = explode(":",$tempC[1]);
        $tempototalmoduloC = (($horaC[0]+1) * 60) + $horaC[1];  
        $tempototalmoduloC = strtotime($arenav2GPRS->DATA);

        $tempD = explode(" ",$arenav2SATELITAL->DATA);
        $horaD = explode(":",$tempD[1]);

        $tempototalmoduloD = (($horaD[0]+1) * 60) + $horaD[1];  
        $tempototalmoduloD = strtotime($arenav2SATELITAL->DATA);

        // CALCULO DO TEMPO DOS MODULOS.
        $ARENAV2_GPRS = ($tempototalserv - $tempototalmoduloC);
        $ARENAV2_SATELITAL = ($tempototalserv - $tempototalmoduloD);

        // REGRAS DAS MANSAGENS       
        $erros = [
            'CHECKPOINT' => 0,
            'ARENAV2_GPRS' => 0,
            'ARENAV2_SATELITAL' => 0,
            'MIRROR' => 0,
            'ARENA' => 0,
            'ERRO' => ''
        ];

        // verifica o mirror esta funcionando
        // nunca pode estar com 0 senao inicia o alerta;
        // if (! $numConexao) {
        //     $erros['MIRROR'] = 1;
        //     return $erros;
        // }
        
        // 900 segundos = 15 minutos
        if ($CHECKPOINT_CLARO > 900 && $CHECKPOINT_TIM  > 900) {
            $erros['CHECKPOINT'] = 1;
        } 

        // 900 segundos = 15 minutos
        if ($ARENAV2_GPRS > 900) {
            $erros['ARENAV2_GPRS'] = 1;
        }

        //quantidade de xml na pasta sateltal
        // 1800 = 30 minutos 
        $qnt = $this->consultaQntXml();
        if ($qnt > 10) {
            $erros['ARENAV2_SATELITAL'] = '1';
        }

        // verifica se o mirror esta == a 1 se tiver esta com erro
        if($numConexao){
            if ($numConexao->MIRROR_0 <= '0') {
                $erros['MIRROR'] = 1;
            }

            // pega a quantidade de conexoes dos arenas
            if (($numConexao->ARENA_1 == '0') || ($numConexao->ARENA_2 == '0') || ($numConexao->ARENA_3 == '0')) {
                $erros['ARENA'] = 1;
            }
        } else {
            $erros['ERRO'] = StatusWebService::DADOSARENA;
        }

        return $erros;
    }

}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class XML_Model extends CI_Model
{
    protected $CABECALHO_XML = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";

    public function __construct()
    {
        parent::__construct();
        $this->load->library('StatusWebService');
    }

    public function findDados($login, $senha)
    {
        
        $ARENA_1 = StatusWebService::ARENA_1;
        $ARENA_2 = StatusWebService::ARENA_2;
        $ARENA_3 = StatusWebService::ARENA_3;

        $arena = array(
            $ARENA_1,
            $ARENA_2,
            $ARENA_3
        );

        return $this->dadosXML($login, $senha, $arena);
    }

    private function dadosXML($login, $senha, $arena) 
    {
        $XML  = $this->CABECALHO_XML;
        $XML .= "<USER>";
        $XML .= "<LOGIN>$login</LOGIN>";
        $XML .= "<PASSWORD>$senha</PASSWORD>";
        $XML .= "<FORCELOGIN>1</FORCELOGIN>";
        $XML .= "<JUST_VERIFY>0</JUST_VERIFY>";
        $XML .= "</USER>";

        $data = array(
            'arena_0' => '',
            'arena_1' => '',
            'arena_2' => ''
        );

        $num=0;
        
        foreach ($arena as $value) {
            $data['arena_'.$num] = $this->consulta($value, $XML);
            $num++;
        }
        
        return $data;
    }

    private function consulta($end, $XML)
    {
        $Response = '';
        $Req = new HTTP_Request($end . 'login');
        $Req->setMethod(HTTP_REQUEST_METHOD_POST);
        $Req->clearPostData();
        $Req->addRawPostData($XML,True);
        $Req->sendRequest();
        $Response = $Req->getResponseBody();
        $Response = str_replace("\\", "", $Response);
        
        $DadosResposta = readXML($Response,"USER");
        $DadosResposta = $DadosResposta[0];
        $Sessao = $Req->getResponseCookies();

        return $DadosResposta;
    }
}
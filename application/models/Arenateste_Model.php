<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Arenateste_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('StatusWebService');
        $this->load->model('ArenaV2_Model');
        date_default_timezone_set('America/Sao_Paulo');
    }

    public function cabecalho()
    {
            $dtA = '2021-02-20 00:00:00';
            $dtB = '2021-02-24 23:59:59';
            $limite = '1';
            $serial = '8171937';
            $id = '7783';

            $XML  = "<COURSES>";
            $XML .= "<VEHICLES>";
            $XML .= "<VEHICLE_ID>$id</VEHICLE_ID>";
            $XML .= "</VEHICLES>";
            $XML .= "<START>$dtA</START>";
            $XML .= "<END>$dtB</END>";
            $XML .= "<PERIOD_PER_DAY>0</PERIOD_PER_DAY> ";
            $XML .= "<DECREASING>1</DECREASING>";
            $XML .= "<LIMIT>";
            $XML .= "<COUNT>1</COUNT>";
            $XML .= "<QUANT>$limite</QUANT>";
            $XML .= "<OFFSET>0</OFFSET>";
            $XML .= "</LIMIT>";
            $XML .= "<SHOW_FIELDS>";
            $XML .= "<ID>1</ID>";
            $XML .= "<DESC>1</DESC>";
            $XML .= "<TYPE_ID>1</TYPE_ID>";
            $XML .= "<TYPE_DESC>1</TYPE_DESC>";
            $XML .= "<CLI_DESC>1</CLI_DESC>";
            $XML .= "<DATE_TIME>1</DATE_TIME>";
            $XML .= "<Y>1</Y>";
            $XML .= "<X>1</X>";
            $XML .= "<DIR>1</DIR>";
            $XML .= "<VEL>1</VEL>";
            $XML .= "<IGNITION>1</IGNITION>";
            $XML .= "<NEAR_DESC>1</NEAR_DESC>";
            $XML .= "<ODOMETER>1</ODOMETER>";
            $XML .= "<HOURMETER>1</HOURMETER>";
            $XML .= "<OUT1>1</OUT1>";
            $XML .= "<OUT2>1</OUT2>";
            $XML .= "<OUT3>1</OUT3>";
            $XML .= "<OUT4>1</OUT4>";
            $XML .= "<COUNT1>1</COUNT1>";
            $XML .= "<COUNT2>1</COUNT2>";
            $XML .= "<COUNT3>1</COUNT3>";
            $XML .= "<NAME_OUT1>1</NAME_OUT1>";
            $XML .= "<NAME_OUT2>1</NAME_OUT2>";
            $XML .= "<NAME_OUT3>1</NAME_OUT3>";
            $XML .= "<NAME_OUT4>1</NAME_OUT4>";
            $XML .= "<IN1>1</IN1>";
            $XML .= "<NAME_IN1>1</NAME_IN1>";
            $XML .= "<IN2>1</IN2>";
            $XML .= "<NAME_IN2>1</NAME_IN2>";
            $XML .= "<IN3>1</IN3>";
            $XML .= "<NAME_IN3>1</NAME_IN3>";
            $XML .= "<TIME_EXCESS>1</TIME_EXCESS>";
            $XML .= "<VOLTAGE>1</VOLTAGE>";
            $XML .= "<ALERT>1</ALERT>";
            $XML .= "<PROTOCOL>1</PROTOCOL>";
            $XML .= "</SHOW_FIELDS>";
            $XML .= "</COURSES>";

            return $XML;
    }

    //#####################################   BANCO 1    ####################################
    public function arena_0()
    {
        $XML = $this->cabecalho();
        $Response = '';
        $ReqVeiculos = new HTTP_Request(get_webservice_0()."mapcourses?WebBrokerSessionID=".get_userID_arena_0());
        $ReqVeiculos->setMethod(HTTP_REQUEST_METHOD_POST);
        $ReqVeiculos->clearPostData();
        $ReqVeiculos->addRawPostData($XML,True);
        $ReqVeiculos->sendRequest();
        $Response = $ReqVeiculos->getResponseBody();
        
        return readXML($Response,"COURSES")[0];  
    }
}
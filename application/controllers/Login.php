<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->load->model('XML_Model');
        $this->load->library('StatusWebService');

        header("Pragma: no-cache"); // HTTP/1.0
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
        header("Cache-Control: post-check=0, pre-check=0",False);
        header("Cache-Control: no-store, no-cache, must-revalidate"); // HTTP/1.1
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
    }

    public function index()
    {
        $info['msg'] = "";
        $login = $this->input->post('login');
        $senha = $this->input->post('senha');

        if ($login && $senha) {
            $dados = $this->consultaXML($login, $senha);

            if ($dados['arena_0'] != NULL || $dados['arena_1'] !== NULL || $dados['arena_2'] !== NULL ) {
                set_user_data($dados);  
                $this->setLoginSession($login);
                $this->webservice();
            } else {
                $info['msg'] = 'Login ou Senha incorretos!';               
            }
        }

        $dadosIdUsuario = isset(get_user_data()['arena_0']['ID']);

        if ($dadosIdUsuario) {
            return $this->redirect->url('welcome')
                ->withSuccess('Usuário logado!')
                ->send();
        }

        $this->template->view('login', $info);
    }

    private function consultaXML($login, $senha)
    {   
        $dados = $this->XML_Model->findDados($login, $senha);
        return $dados;
    }

    public function logout()
    {
        $this->session->sess_destroy();
        $info['msg'] = 'Logout realizado!';
        $this->template->view('login', $info);
	}
    private function setLoginSession($login)
    {
        $dadosLogin = array(
           'login' => $login
        );

        set_user_data($dadosLogin);
    }

    public function webservice()
    {

        $ARENA_1 = StatusWebService::ARENA_1;
        $ARENA_2 = StatusWebService::ARENA_2;
        $ARENA_3 = StatusWebService::ARENA_3;

        $webservice = array(
            $ARENA_1,
            $ARENA_2,
            $ARENA_3
        );                    

        $data = array(
           'webservice_0' => '',
           'webservice_1' => '',
           'webservice_2' => ''
        );

        $num=0;
        
        foreach ($webservice as $value) {
            $data['webservice_'.$num] = $value;
            $num++;
        }

        set_user_data($data);
    }
}
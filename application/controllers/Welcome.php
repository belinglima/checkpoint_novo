<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller 
{
    public function __construct()
    { 
        parent::__construct();
        $this->load->model('ArenaV2_Model');
        $this->load->model('Arena_Model');
        $this->load->library('StatusWebService');
        
        if (! isset(get_user_data()['arena_0']['ID'])) {
            return $this->redirect->url('login/logout')
                ->send();
        }
    }

    public function index()
    {
        $info['dados'] = $this->consultaDados();
        $info['data'] = $this->Arena_Model->logica();
        $info['conexoes'] = $this->Arena_Model->consultaNumerosConexao();
        $info['endereco'] = StatusWebService::DADOSARENA;
        
        $this->template->view('bemvindo', $info);
    }

    public function consultaDados()
    {
        $arena_0 = $this->Arena_Model->arena_0();
        $arena_1 = $this->Arena_Model->arena_1();
        $arena_2 = $this->Arena_Model->arena_2();

        if ($arena_0 || $arena_1 || $arena_2) {
            $info['claro'] = isset($arena_0[0]["DESC"]) ? $arena_0[0]["DESC"] : 'CHECKPOINT - CLARO - ERRO...';
            $info['banco_1_claro'] = isset($arena_0[0]["DATE_TIME"]) ? $arena_0[0]["DATE_TIME"] : '1970/01/01 00:00:00h';
            $info['banco_2_claro'] = isset($arena_1[0]["DATE_TIME"]) ? $arena_1[0]["DATE_TIME"] : '1970/01/01 00:00:00h';
            $info['banco_3_claro'] = isset($arena_2[0]["DATE_TIME"]) ? $arena_2[0]["DATE_TIME"] : '1970/01/01 00:00:00h';

            $info['tim'] = isset($arena_0[2]["DESC"]) ? $arena_0[2]["DESC"] : 'CHECKPOINT-TIM - ERRO...';
            $info['banco_1_tim'] = isset($arena_0[2]["DATE_TIME"]) ? $arena_0[2]["DATE_TIME"] : '1970/01/01 00:00:00h';
            $info['banco_2_tim'] = isset($arena_1[2]["DATE_TIME"]) ? $arena_1[2]["DATE_TIME"] : '1970/01/01 00:00:00h';
            $info['banco_3_tim'] = isset($arena_2[2]["DATE_TIME"]) ? $arena_2[2]["DATE_TIME"] : '1970/01/01 00:00:00h';
        
            $info['sinc_Claro'] = ($info['banco_1_claro'] == ($info['banco_2_claro'] == $info['banco_3_claro'])) ? 0 : 1; 
            $info['sinc_Tim'] = ($info['banco_1_tim'] == ($info['banco_2_tim'] == $info['banco_3_tim'])) ? 0 : 1;
        } else {
            $info['claro'] = 'CHECKPOINT - CLARO';
            $info['banco_1_claro'] = '1970/01/01 00:00:00h';
            $info['banco_2_claro'] = '1970/01/01 00:00:00h';
            $info['banco_3_claro'] = '1970/01/01 00:00:00h';

            $info['tim'] = 'CHECKPOINT-TIM';
            $info['banco_1_tim'] = '1970/01/01 00:00:00h';
            $info['banco_2_tim'] = '1970/01/01 00:00:00h';
            $info['banco_3_tim'] = '1970/01/01 00:00:00h';

            $info['sinc_Claro'] = 1;
            $info['sinc_Tim'] = 1;

            // echo "Linha 60 Controller Welcome - Erro no Arena, Web-Serivce<pre>";
        }

        $dataAMGPRS = $this->ArenaV2_Model->consultaGRPS()->DATA;
        $dataAMSATELITAL = $this->ArenaV2_Model->consultaSATELITAL()->DATA;

        if ($dataAMGPRS || $dataAMSATELITAL) {
            $info['ArenaV2_gprs'] = $dataAMGPRS;
            $info['ArenaV2_satelital'] = $dataAMSATELITAL;
            $info['alertaGPRS'] = $this->mediaGPRS($dataAMGPRS);    
        } else {
            $info['ArenaV2_gprs'] = 1;
            $info['ArenaV2_satelital'] = 1;
            $info['alertaGPRS'] = 1;
            
            echo "Linha 72 - Controller Welcome - Erro de Leitura do banco Arena Master<pre>";
        }
        
        return $info;
    }

    public function mediaGPRS($dataAM_GPRS)
    {
        $abertura = new DateTime(date('Y-m-d H:i:s'));
        $diff      = $abertura->diff(new DateTime($dataAM_GPRS));
        $minutos   = $diff->i;
        $media =  sprintf('%02d', $minutos);

        if ($media > '15') {
            return '1';
        }

        return '0';
    }
}
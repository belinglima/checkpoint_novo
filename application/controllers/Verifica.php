<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Verifica extends CI_Controller 
{
    public function __construct()
    { 
        parent::__construct();
        $this->load->model('Arenateste_Model');
        
        if (! isset(get_user_data()['arena_0']['ID'])) {
            return $this->redirect->url('login/logout')
                ->send();
        }
    }

    public function index()
    {
        $teste = $this->Arenateste_Model->arena_0();

        echo "<pre>";
        var_dump($teste);
        die;
    }
}
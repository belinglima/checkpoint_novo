<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>.:: CheckPoint ::.</title>
	<meta http-equiv="refresh" content="30;url=welcome" />
	<link rel="shortcut icon" 
		href="{{ base_url('assets/images/favicon.ico') }}" type="image/x-icon">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="theme-color" content="#1a237e">
	@include('template.css')
</head>
<body>
	<main>
		@include('template.menu-topo')
		@yield('content')
	</main>
</body>
<script type="text/javascript">
	function base_url(caminho) {
		return '{{ base_url() }}' + caminho;
	}
</script>
@include('template.javascript')
</html>
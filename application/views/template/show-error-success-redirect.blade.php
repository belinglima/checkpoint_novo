@if (!!get_success_redirect())
	<div class="chip fundoamarelo">
        <span class="textoroxo">{!! get_success_redirect() !!}</span>
        <i class="close material-icons textoroxo left" style="margin-left: -8px; margin-right: 12px;">close</i>
    </div>
@elseif (!!get_error_redirect())
    <div class="chip fundovermelho">
        <span class="textovermelho">{!! get_error_redirect() !!}</span>
        <i class="close material-icons textovermelho left" style="margin-left: -8px; margin-right: 12px;">close</i>
    </div>
@endif
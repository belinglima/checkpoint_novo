@extends('template.base') 

@section('content')

@php
	$str = array('<second>', '</second>');

	$bancoArena1 = $conexoes ? str_replace($str, "", $conexoes->ARENA_1) : false;
	$bancoArena2 = $conexoes ? str_replace($str, "", $conexoes->ARENA_2) : false;
	$bancoArena3 = $conexoes ? str_replace($str, "", $conexoes->ARENA_3) : false;

	if (!$bancoArena1){
		$bancoArena1 = '0';
	}
	
	if (!$bancoArena2){
		$bancoArena2 = '0';
	}
	
	if (!$bancoArena3){
		$bancoArena3 = '0';
	}

	if ($conexoes) {
		$mirrorData = ($conexoes->MIRROR_0 > 0)  ? $conexoes->MIRROR_0 : 0;
	} else {
		$mirrorData = 0;
	}

	$bancosTim = [
		1 => '',
		2 => '',
		3 => ''
	];

	$SYNC_TIMMMM = 0;
	$SYNC_CLAROOOO = 0;

	$ontem = date('Y-m-d H:i:s', strtotime('-15 minutes'));

	for ($i = 1; $i <=3 ; $i++) :
		if ( $dados['banco_'.$i.'_tim'] < $ontem) :
			$SYNC_TIMMMM = '1';
			$bancosTim[$i] = "red lighten-4";
		endif;
	endfor;

	$bancosClaro = [
		1 => '',
		2 => '',
		3 => ''
	];

	for ($i = 1; $i <=3 ; $i++) :
		if ( $dados['banco_'.$i.'_claro'] < $ontem) :
			$SYNC_CLAROOOO = '1';
			$bancosClaro[$i] = "red lighten-4";
		endif;
	endfor;

	function ping($host) {
        exec(sprintf('ping -c 1 -W 5 %s', escapeshellarg($host)), $res, $rval);
        return $rval === 0;
	}

	$arrayIps = array();
	$color = "green";
	$alert = "";
	$names = array('OI_PRIMÁRIO', '227', '240', '245', '250');
	$host = array('200.102.219.67', '186.215.192.227', '186.215.192.240', '186.215.192.245', '186.215.192.250');

	for ($j = 0; $j < count($host); $j++) :
		$ips = new stdClass;
		if( ping( $host[$j] ) ) {
			$ips->name = $names[$j];
			$ips->status = ping($host[$j]);
			$ips->cor = "green";
			$ips->alerta = '0';
		} else {
			$ips->name = $names[$j];
			$ips->status = ping($host[$j]);
			$ips->cor = "red darken 2";
			$color = "red darken 2";
			$alert = "pulse";
			$ips->alerta = '1';
		}
		$arrayIps[$j] = $ips;
	endfor;
@endphp

<div class="container">
	<div class="hide">
		<audio id="audio" controls>
		   <source src="{{ base_url('assets/songs/ALERTA.mp3') }}" type="audio/mp3" />
		</audio>
	</div>

	<div class="row">

		@if(!$mirrorData)
			<div class="col s12" style="border-radius: 12px; margin-top: 10px; margin-bottom: 20px;">
				<div class="card red darken-2 white-text">
					<div class="card-content center-align">
						<h5>
							IIS sem comunicação necessário reiniciar.
						</h5>
						<a href="{{$data['ERRO']}}" target="_blank">ERRO 500</a>
					</div>
				</div>
			</div>
		@endif


		<div class="col s6">

			<div class="card green lighten-3 sucesso-arena" style="border-radius: 12px; margin-top: -14px;">
				<div class="card-content center-align">
					<i class="material-icons large green-text text-darken-4">done</i>
					<h5>ARENA</h5>
				</div>
			</div>	

			<div class="card red darken-2 deu-erro-arena hide" style="border-radius: 12px; margin-top: -14px;">
				<div class="card-content center-align">
					<i class="material-icons large white-text">warning</i>
					<h5 class="white-text">ARENA</h5>
					<p class="white-text">
						<b>
							{{ $SYNC_CLAROOOO == 1 ? 'SOMENTE ALERTA SEM DISPARO' : '' }}
						</b>
					</p>
				</div>
			</div>

			<div class="card" style="margin-top: 10px;">
				<div class="card-content center-align">
					<table>
						<tbody>
							<tr>
								<td colspan="2"><b>{{ $dados['claro'] }}</b></td>
							</tr>
							<tr class="{{ $bancosClaro[1] }}">
								<td><b>BANCO 1</b></td>
								<td>{{ format_datetime_to_show($dados['banco_1_claro']) }}h</td>
							</tr>
							<tr class="{{ $bancosClaro[2] }}">
								<td><b>BANCO 2</b></td>
								<td>{{ format_datetime_to_show($dados['banco_2_claro']) }}h</td>
							</tr>
							<tr class="{{ $bancosClaro[3] }}">
								<td><b>BANCO 3</b></td>
								<td>{{ format_datetime_to_show($dados['banco_3_claro']) }}h</td>
							</tr>
						</tbody>
					</table>
					<p>
						<b>
							{{ $dados['sinc_Claro'] == 1 ? '*** Dados dessincronizados ***' : '' }}
						</b>
					</p>
					<p>
						<b>
							{{ $SYNC_CLAROOOO == 1 ? '*** Com erro a mais de 15 minutos ***' : '' }}
						</b>
					</p>
				</div>
			</div>

			<div class="card" style="margin-top: 10px;">
				<div class="card-content center-align">
					<table>
						<tbody>
							<tr>
								<td colspan="2"><b>{{ $dados['tim'] }}</b></td>
							</tr>
							<tr class="{{ $bancosTim[1] }}">
								<td><b>BANCO 1</b></td>
								<td>{{ format_datetime_to_show($dados['banco_1_tim']) }}h</td>
							</tr>
							<tr class="{{ $bancosTim[2] }}">
								<td><b>BANCO 2</b></td>
								<td>{{ format_datetime_to_show($dados['banco_2_tim']) }}h</td>
							</tr>
							<tr class="{{ $bancosTim[3] }}">
								<td><b>BANCO 3</b></td>
								<td>{{ format_datetime_to_show($dados['banco_3_tim']) }}h</td>
							</tr>
						</tbody>
					</table>
					<p></p>
					<p>
						<b>
							{{ $dados['sinc_Tim'] == 1 ? '*** Dados dessincronizados ***' : '' }}
						</b>
					</p>
					<p>
						<b>
							{{ $SYNC_TIMMMM == 1 ? '*** Com erro a mais de 15 minutos ***' : '' }}
						</b>
					</p>
				</div>
			</div>
		</div>

		<div class="col s6">

			<div class="card green lighten-3 sucesso-am" style="border-radius: 12px; margin-top: -14px;">
				<div class="card-content center-align">
					<i class="material-icons large green-text text-darken-4">done</i>
					<h5>AM</h5>
				</div>
			</div>	

			<div class="card red darken-2 deu-erro-am hide" style="border-radius: 12px; margin-top: -14px;">
				<div class="card-content center-align">
					<i class="material-icons large white-text">warning</i>
					<h5 class="white-text">AM</h5>
					<p class="white-text">
						<b>
							{{ $SYNC_CLAROOOO == 1 ? 'SOMENTE ALERTA SEM DISPARO' : '' }}
						</b>
					</p>
				</div>
			</div>

			<div class="card" style="margin-top: 10px;">
				<div class="card-content center-align">
					<table>
						<tbody>
							<tr>
								<td><b>GPRS</b></td>
								<td>{{ format_datetime_to_show($dados['ArenaV2_gprs']) }}h</td>
							</tr>
							<tr>
								<td><b>SATELITAL</b></td>
								<td>{{ format_datetime_to_show($dados['ArenaV2_satelital']) }}h</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

			<div class="card {{ $mirrorData == 0 ? 'red darken-2 white-text' : '' }}" style="margin-top: 10px;">
				<div class="card-content center-align">
					<b>Número de Conexões</b>
					<table>
						<tbody>
							<tr>
								<td><b>MIRROR</b></td>
								<td colspan="2">{{ $mirrorData }}</td>
							</tr>
							<tr>
								<td><b>BANCO ARENA 1</b></td>
								<td>{{ ($bancoArena1 !== null) ? $bancoArena1 : "PARADO" }}</td>
							</tr>
							<tr>
								<td><b>BANCO ARENA 2</b></td>
								<td>{{ ($bancoArena2 !== null) ? $bancoArena2 : "PARADO" }}</td>
							</tr>
							<tr>
								<td><b>BANCO ARENA 3</b></td>
								<td>{{ ($bancoArena3 !== null) ? $bancoArena3 : "PARADO" }}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

		</div>

		<div class="fixed-action-btn toolbar direction-top">
	      	<a class="btn-floating btn-large {{ $color }} {{ $alert }} ">
	        	<i class="large material-icons">iso</i>
	      	</a>
	      	<ul>
	      		@foreach($arrayIps AS $key => $value)
			        <li class="{{ $arrayIps[$key]->cor }}">
			        	<a class="waves-effect waves-light btn">{{ $arrayIps[$key]->name }}</a>
			        </li>

			       	@php
						if($arrayIps[$key]->alerta == '1'){
							echo "<script> document.addEventListener('DOMContentLoaded', function(){ audio.play(); }); </script>";
							break;
						}
					@endphp
	      		@endforeach
	      	</ul>
	    </div>

	</div>

</div>

@stop

@section('extra-javascript')
	<script type="text/javascript">
	    document.addEventListener('DOMContentLoaded', function() {
		    var elems = document.querySelectorAll('.fixed-action-btn');
		    var instances = M.FloatingActionButton.init(elems, {
		      toolbarEnabled: true
		    });
		});

		let CHECKPOINT = '{{ $data['CHECKPOINT'] }}';
		let ARENAV2_GPRS = '{{ $data['ARENAV2_GPRS'] }}';
		let ARENAV2_SATELITAL = '{{ $data['ARENAV2_SATELITAL'] }}';
		let MIRROR = '{{ $data['MIRROR'] }}';
		let ARENA = '{{ $data['ARENA'] }}';
		let SYNC_CLARO = '{{ $dados['sinc_Claro'] }}';
		let SYNC_TIM = '{{ $dados['sinc_Tim'] }}';
		let ALERTA_GPRS = '{{ $dados['alertaGPRS'] }}';
		let CON_ARENA_1 = '{{ $bancoArena1 }}';
		let CON_ARENA_2 = '{{ $bancoArena2 }}';
		let CON_ARENA_3 = '{{ $bancoArena3 }}';
		let CON_MIRROR = '{{ $mirrorData }}';
		var TIM = '{{ $SYNC_TIMMMM }}';
		var CLARO = '{{ $SYNC_CLAROOOO }}';
		var ERRO = '{{ $data['ERRO'] }}';
		var URL = '{{ $endereco }}';

		let dados = {
			'SYNC_TIM': SYNC_TIM,
			'SYNC_CLARO': SYNC_CLARO,
			'CHECKPOINT': CHECKPOINT,
			'MIRROR': MIRROR,
			'ARENA': ARENA,
			'ARENAV2_GPRS': ARENAV2_GPRS,
			'ARENAV2_SATELITAL': ARENAV2_SATELITAL,
			'ALERTA_GPRS': ALERTA_GPRS
		};

		console.log(ERRO)

		console.table(dados);

		let dados1 = {
			'CON_ARENA_1': CON_ARENA_1,
			'CON_ARENA_2': CON_ARENA_2,
			'CON_ARENA_3': CON_ARENA_3,
			'CON_MIRROR': CON_MIRROR,
			'URL':URL
		};

		console.table(dados1);

		let dados2 = {
			'CON_ARENA_0[2] - ERRO TIM': TIM,
			'CON_ARENA_0[0] - ERRO CLARO': CLARO
		};
		
		console.table(dados2);

		const chamaErroArena = (resposta) => {
			if (TIM == '1' && CLARO == '1') {
				$(".sucesso-arena").toggleClass("hide");
				$(".deu-erro-arena").toggleClass("hide");
			} 
			if (resposta) {
				audio.play();
				$(".sucesso-arena").toggleClass("hide");
				$(".deu-erro-arena").toggleClass("hide");
			}
        }

		const chamaErroAM = () => {
			audio.play();
			$(".sucesso-am").toggleClass("hide");
			$(".deu-erro-am").toggleClass("hide");
		}

	  	var audio = document.getElementById('audio');
	  	document.getElementById('audio').muted = false;

		if (CHECKPOINT == '1' || 
			MIRROR == '1' || 
			ARENA == '1' ||
			CON_ARENA_1 == '0' || 
			CON_ARENA_2 == '0' || 
			CON_ARENA_3 == '0'
		) {
			chamaErroArena(true);
		} else {
			chamaErroArena(false);
		}

		if (ARENAV2_GPRS == '1' || 
			ARENAV2_SATELITAL == '1' || 
			ALERTA_GPRS == '1'
		) {
			chamaErroAM();
		}
	</script>
@stop
@extends('template.base') 

@section('content')

{{-- @php
	$str = array('<second>', '</second>');
	$bancoArena1 = $conexoes ? str_replace($str, "", $conexoes->ARENA_1) : false;
	$bancoArena2 = $conexoes ? str_replace($str, "", $conexoes->ARENA_2) : false;
	$bancoArena3 = $conexoes ? str_replace($str, "", $conexoes->ARENA_3) : false;
	if (!$bancoArena1){
		$bancoArena1 = '0';
	}
	
	if (!$bancoArena2){
		$bancoArena2 = '0';
	}
	
	if (!$bancoArena3){
		$bancoArena3 = '0';
	}
	if ($conexoes) {
		$mirrorData = ($conexoes->MIRROR_0 > 0)  ? $conexoes->MIRROR_0 : 0;
	} else {
		$mirrorData = 0;
	}
	$bancosTim = [
		1 => '',
		2 => '',
		3 => ''
	];
	$SYNC_TIMMMM = 0;
	$SYNC_CLAROOOO = 0;
	$ontem = date('Y-m-d H:i:s', strtotime('-15 minutes'));
	for ($i = 1; $i <=3 ; $i++) :
		if ( $dados['banco_'.$i.'_tim'] < $ontem) :
			$SYNC_TIMMMM = '1';
			$bancosTim[$i] = "red lighten-4";
		endif;
	endfor;
	$bancosClaro = [
		1 => '',
		2 => '',
		3 => ''
	];
	for ($i = 1; $i <=3 ; $i++) :
		if ( $dados['banco_'.$i.'_claro'] < $ontem) :
			$SYNC_CLAROOOO = '1';
			$bancosClaro[$i] = "red lighten-4";
		endif;
	endfor;
	function ping($host) {
        exec(sprintf('ping -c 1 -W 5 %s', escapeshellarg($host)), $res, $rval);
        return $rval === 0;
	}
	$arrayIps = array();
	$color = "green";
	$alert = "";
	$names = array('OI_PRIMÁRIO', '227', '240', '245', '250');
	$host = array('200.102.219.67', '186.215.192.227', '186.215.192.240', '186.215.192.245', '186.215.192.250');
	for ($j = 0; $j < count($host); $j++) :
		$ips = new stdClass;
		if( ping( $host[$j] ) ) {
			$ips->name = $names[$j];
			$ips->status = ping($host[$j]);
			$ips->cor = "green";
			$ips->alerta = '0';
		} else {
			$ips->name = $names[$j];
			$ips->status = ping($host[$j]);
			$ips->cor = "red darken 2";
			$color = "red darken 2";
			$alert = "pulse";
			$ips->alerta = '1';
		}
		$arrayIps[$j] = $ips;
	endfor;
@endphp --}}

{{-- <div class="container"> --}}
	{{-- <div class="hide">
		<audio id="audio" controls>
		   <source src="{{ base_url('assets/songs/ALERTA.mp3') }}" type="audio/mp3" />
		</audio>
	</div> --}}

	<div class="row full-width padding fundoroxo textoamarelo">
    	<div class="col s12 m8 left">
	        <h4 class="header">Check Point</h4>

	        <div class="left-align">
	        	Última Atualização <b> {{ date("H:i:s - d/m/Y") }}</b>
	    	</div>
	    </div>

    	<div class="col s12 m2 right" style="margin-top: 20px;">

		    <a class="btn fundoamarelo textoroxo col s12 m4" style="margin-right: 5px; margin-bottom: 5px;" href="{{ base_url('login/logout') }}">
		    	{{-- <div > --}}
				    <i class="material-icons">logout</i>
				{{-- </div> --}}
			</a>

			<div class="btn fundoverde col s12 m4 botaoAlerta modal-trigger" href="#modalAlerta">
		    	{{-- <div > --}}
				    <i class="material-icons">warning</i>
				{{-- </div> --}}
			</div>

		</div>
	</div>

	 <div class="row center-align textoroxo topo">
    	<div class="col s6 m2 borda fundoverde tamanhopadrao paddingtopo bold">
    		227
    	</div>
    	<div class="col s6 m2 borda fundoverde tamanhopadrao paddingtopo bold">
    		240
    	</div>
    	<div class="col s6 m2 borda fundoverde tamanhopadrao paddingtopo bold">
    		243
    	</div>
    	<div class="col s6 m2 borda fundoverde tamanhopadrao paddingtopo bold">
    		245
    	</div>
    	<div class="col s6 m2 borda fundoverde tamanhopadrao paddingtopo bold">
    		250
    	</div>
    	<div class="col s6 m2 borda fundoverde tamanhopadrao paddingtopo bold">
    		OI PRIMARIO
    	</div>
    </div>

    <div class="row center-align textoroxo topo">
    	
    	<div class="col s12 m4 fundoverde bold fim borda">
    		<p class="col s12 fundoroxo textoamarelo full-width borda">Arena</p>
    		
    		<div class="col s6 borda">
    			<p class="col s12 fundoroxo textoamarelo full-width topoinside borda">Claro</p>

    			<p class="textopequeno">BANCO 1	01/01/1970 00:00:00h</p>
    			<p class="textopequeno">BANCO 2	01/01/1970 00:00:00h</p>
    			<p class="textopequeno">BANCO 3	01/01/1970 00:00:00h</p>
    		</div>
    		<div class="col s6 borda">
    			<p class="col s12 fundoroxo textoamarelo full-width topoinside borda">Tim</p>

    			<p class="textopequeno">BANCO 1	01/01/1970 00:00:00h</p>
    			<p class="textopequeno">BANCO 2	01/01/1970 00:00:00h</p>
    			<p class="textopequeno">BANCO 3	01/01/1970 00:00:00h</p>
    		</div>
    	</div>

    	<div class="col s12 m4 fundoverde bold fim2 borda">
    		<p class="col s12 fundoroxo textoamarelo full-width borda">Arena v2</p>
    		
    		<div class="col s6 borda">
    			<p class="col s12 fundoroxo textoamarelo full-width topoinside borda">GPRS</p>

    			<p class="textopequeno">15/08/2021 21:19:14h</p>
    		</div>
    		<div class="col s6 borda">
    			<p class="col s12 fundoroxo textoamarelo full-width topoinside borda">Satelital</p>

    			<p class="textopequeno">15/08/2021 21:19:02h</p>
    		</div>
    	</div>

    	<div class="col s12 m4 fundoverde bold fim2 borda">
    		<p class="col s12 fundoroxo textoamarelo full-width borda">Suntech</p>
    		
    		<div class="col s12 borda">
    			<p class="col s12 fundoroxo textoamarelo full-width topoinside borda">Gateway</p>

    			<p class="textopequeno">01/01/1970 00:00:00h</p>
    		</div>
    	</div>

    </div>

     <div class="row center-align textoroxo topo">
     	<div class="col s12 fundoverde bold fim borda">
    		<p class="col s12 fundoroxo textoamarelo full-width borda">Número de Conexões</p>

    		<div class="col s12 m3 borda">
    			<p class="col s12 fundoroxo textoamarelo full-width topoinside borda">MIRROR</p>

    			<p class="textopequeno padding">01</p>
    		</div>

    		<div class="col s12 m3 borda">
    			<p class="col s12 fundoroxo textoamarelo full-width topoinside borda">ARENA 1</p>

    			<p class="textopequeno padding">01</p>
    		</div>

    		<div class="col s12 m3 borda">
    			<p class="col s12 fundoroxo textoamarelo full-width topoinside borda">ARENA 2</p>

    			<p class="textopequeno padding">01</p>
    		</div>

    		<div class="col s12 m3 borda">
    			<p class="col s12 fundoroxo textoamarelo full-width topoinside borda">ARENA 3</p>

    			<p class="textopequeno padding">01</p>
    		</div>
    	</div>
     </div>

	{{-- <div class="row">
		{{-- <div class="col s12 red"> --}}
			{{-- <p>teste</p> --}}
		{{-- </div> --}}

		{{-- @if(!$mirrorData)
			<div class="col s12" style="border-radius: 12px; margin-top: 10px; margin-bottom: 20px;">
				<div class="card red darken-2 white-text">
					<div class="card-content center-align">
						<h5>
							IIS sem comunicação necessário reiniciar.
						</h5>
						<a href="{{$data['ERRO']}}" target="_blank">ERRO 500</a>
					</div>
				</div>
			</div>
		@endif --}}


		{{-- <div class="col s6">
			<div class="card green lighten-3 sucesso-arena" style="border-radius: 12px; margin-top: -14px;">
				<div class="card-content center-align">
					<i class="material-icons large green-text text-darken-4">done</i>
					<h5>ARENA</h5>
				</div>
			</div>	
			<div class="card red darken-2 deu-erro-arena hide" style="border-radius: 12px; margin-top: -14px;">
				<div class="card-content center-align">
					<i class="material-icons large white-text">warning</i>
					<h5 class="white-text">ARENA</h5>
					<p class="white-text">
						<b>
							{{ $SYNC_CLAROOOO == 1 ? 'SOMENTE ALERTA SEM DISPARO' : '' }}
						</b>
					</p>
				</div>
			</div>
			<div class="card" style="margin-top: 10px;">
				<div class="card-content center-align">
					<table>
						<tbody>
							<tr>
								<td colspan="2"><b>{{ $dados['claro'] }}</b></td>
							</tr>
							<tr class="{{ $bancosClaro[1] }}">
								<td><b>BANCO 1</b></td>
								<td>{{ format_datetime_to_show($dados['banco_1_claro']) }}h</td>
							</tr>
							<tr class="{{ $bancosClaro[2] }}">
								<td><b>BANCO 2</b></td>
								<td>{{ format_datetime_to_show($dados['banco_2_claro']) }}h</td>
							</tr>
							<tr class="{{ $bancosClaro[3] }}">
								<td><b>BANCO 3</b></td>
								<td>{{ format_datetime_to_show($dados['banco_3_claro']) }}h</td>
							</tr>
						</tbody>
					</table>
					<p>
						<b>
							{{ $dados['sinc_Claro'] == 1 ? '*** Dados dessincronizados ***' : '' }}
						</b>
					</p>
					<p>
						<b>
							{{ $SYNC_CLAROOOO == 1 ? '*** Com erro a mais de 15 minutos ***' : '' }}
						</b>
					</p>
				</div>
			</div> --}}

			{{-- <div class="card" style="margin-top: 10px;">
				<div class="card-content center-align">
					<table>
						<tbody>
							<tr>
								<td colspan="2"><b>{{ $dados['tim'] }}</b></td>
							</tr>
							<tr class="{{ $bancosTim[1] }}">
								<td><b>BANCO 1</b></td>
								<td>{{ format_datetime_to_show($dados['banco_1_tim']) }}h</td>
							</tr>
							<tr class="{{ $bancosTim[2] }}">
								<td><b>BANCO 2</b></td>
								<td>{{ format_datetime_to_show($dados['banco_2_tim']) }}h</td>
							</tr>
							<tr class="{{ $bancosTim[3] }}">
								<td><b>BANCO 3</b></td>
								<td>{{ format_datetime_to_show($dados['banco_3_tim']) }}h</td>
							</tr>
						</tbody>
					</table>
					<p></p>
					<p>
						<b>
							{{ $dados['sinc_Tim'] == 1 ? '*** Dados dessincronizados ***' : '' }}
						</b>
					</p>
					<p>
						<b>
							{{ $SYNC_TIMMMM == 1 ? '*** Com erro a mais de 15 minutos ***' : '' }}
						</b>
					</p>
				</div>
			</div>
		</div> --}}

		{{-- <div class="col s6">
			<div class="card green lighten-3 sucesso-am" style="border-radius: 12px; margin-top: -14px;">
				<div class="card-content center-align">
					<i class="material-icons large green-text text-darken-4">done</i>
					<h5>AM</h5>
				</div>
			</div>	
			<div class="card red darken-2 deu-erro-am hide" style="border-radius: 12px; margin-top: -14px;">
				<div class="card-content center-align">
					<i class="material-icons large white-text">warning</i>
					<h5 class="white-text">AM</h5>
					<p class="white-text">
						<b>
							{{ $SYNC_CLAROOOO == 1 ? 'SOMENTE ALERTA SEM DISPARO' : '' }}
						</b>
					</p>
				</div>
			</div>
			<div class="card" style="margin-top: 10px;">
				<div class="card-content center-align">
					<table>
						<tbody>
							<tr>
								<td><b>GPRS</b></td>
								<td>{{ format_datetime_to_show($dados['ArenaV2_gprs']) }}h</td>
							</tr>
							<tr>
								<td><b>SATELITAL</b></td>
								<td>{{ format_datetime_to_show($dados['ArenaV2_satelital']) }}h</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="card {{ $mirrorData == 0 ? 'red darken-2 white-text' : '' }}" style="margin-top: 10px;">
				<div class="card-content center-align">
					<b>Número de Conexões</b>
					<table>
						<tbody>
							<tr>
								<td><b>MIRROR</b></td>
								<td colspan="2">{{ $mirrorData }}</td>
							</tr>
							<tr>
								<td><b>BANCO ARENA 1</b></td>
								<td>{{ ($bancoArena1 !== null) ? $bancoArena1 : "PARADO" }}</td>
							</tr>
							<tr>
								<td><b>BANCO ARENA 2</b></td>
								<td>{{ ($bancoArena2 !== null) ? $bancoArena2 : "PARADO" }}</td>
							</tr>
							<tr>
								<td><b>BANCO ARENA 3</b></td>
								<td>{{ ($bancoArena3 !== null) ? $bancoArena3 : "PARADO" }}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div> --}}

		{{-- <div class="fixed-action-btn toolbar direction-top">
	      	<a class="btn-floating btn-large {{ $color }} {{ $alert }} ">
	        	<i class="large material-icons">iso</i>
	      	</a>
	      	<ul>
	      		@foreach($arrayIps AS $key => $value)
			        <li class="{{ $arrayIps[$key]->cor }}">
			        	<a class="waves-effect waves-light btn">{{ $arrayIps[$key]->name }}</a>
			        </li>
			       	@php
						if($arrayIps[$key]->alerta == '1'){
							echo "<script> document.addEventListener('DOMContentLoaded', function(){ audio.play(); }); </script>";
							break;
						}
					@endphp
	      		@endforeach
	      	</ul>
	    </div>
 --}}
	{{-- </div> --}}

{{-- </div> --}}

  <!-- Modal Structure -->
  <div id="modalAlerta" class="modal">
    <div class="modal-content fundoroxo textoamarelo full-width center-align">
      <h4>Alerta</h4>
      <h5>Deseja ativar o alerta nos portais?</h5>
      <p>Ao ativar o alerta, os portais de rastreamento ficam avisando <br> para seus usuarios que estamos tendo problemas técnicos!</p>

      <a href="#!" class="btn  fundoverde textoroxo bold borda"> Ativar</a> <a
      href="#!" class="btn  fundoverde textoroxo bold borda">
      Desativar</a> </div> </div>

@stop

@section('extra-javascript')
	<script type="text/javascript">
	 //    document.addEventListener('DOMContentLoaded', function() {
		//     var elems = document.querySelectorAll('.fixed-action-btn');
		//     var instances = M.FloatingActionButton.init(elems, {
		//       toolbarEnabled: true
		//     });
		// });
		$(document).ready(function(){
	    	$('.modal').modal();
	  	});
		
	</script>
@stop